<?php
/**
 * Template Name: Home Template
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
		<div class="c-page-content">
			<?php
			get_template_part( 'assets/views/home-banner' );
			get_template_part( 'assets/views/content-blocks' );
			?>
		</div>
		<?php
	}
}

get_footer();
