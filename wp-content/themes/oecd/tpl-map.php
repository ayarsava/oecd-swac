<?php
/**
 * Template Name: Map Page
 */

get_header();

while ( have_posts() ) {
	the_post();

	get_template_part( 'assets/views/page-header' );
	get_template_part( 'assets/views/sub-nav' );

	?>
	<div class="o-page o-page--tpl-map">
		<?php
		get_template_part( 'assets/views/content-blocks' );
		?>
	</div>
	<?php

}

get_footer();
