<?php
$post_to_output = $args['post_to_output'];

if ( $post_to_output instanceof WP_Post || $post_to_output instanceof stdClass ) {
	$pid = $post_to_output->ID;
} else {
	$pid = $post_to_output;
}

$classes     = $args['classes'] ?? '';
$item_title  = get_the_title( $pid );
$item_link   = get_permalink( $pid );
$single_type = get_post_type_label( $pid );
$date        = get_post_date( $pid );
?>

<a href="<?php echo esc_attr( $item_link ); ?>" title="<?php echo esc_attr( $item_title ); ?>"
   class="c-list__item <?php echo esc_attr( $classes ); ?>">
	<p class="c-list__data">
		<span class="c-list__type"><?php echo esc_html( $single_type ); ?></span>
		<span class="c-list__date"><?php echo esc_html( $date ); ?></span>
	</p>

	<h2 class="c-list__title"><?php echo esc_html( $item_title ); ?></h2>
</a>
