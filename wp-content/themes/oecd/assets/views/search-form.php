<?php
$classes   = $args['classes'] ?? '';
$post_type = $args['post_type'] ?? '';
?>

<form class="c-search-form <?php echo esc_attr( $classes ); ?>" action="<?php echo home_url( '/stories/' ); ?>"
	  method="get" role="search">
	<input class="c-search-form__input" id="search-input" name="find" type="text" placeholder="Search"/>
	<input type="hidden" name="post_type"
		   value="<?php echo $post_type; ?>"/>
	<button class="c-search-form__button" title="Search" type="submit" aria-label="Search">
		<?php
		get_template_part(
			'assets/views/svg',
			null,
			array(
				'classes' => 'c-search-form__icon',
				'icon'    => 'search',
			)
		);
		?>
	</button>
</form>
