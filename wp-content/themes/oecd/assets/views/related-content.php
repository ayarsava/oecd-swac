<?php
$pid             = $args['post_id'] ?? get_the_ID();
$excluded_items  = $args['excluded_items'] ?? array();
$related_content = get_related_content( $pid, 3, $excluded_items );
$description     = get_field( 'description_related_content', $pid ) ?? '';

if ( ! empty( $related_content ) && is_array( $related_content ) ) {
	get_template_part(
		'assets/views/card-block',
		null,
		array(
			'title' => $description,
			'items' => $related_content,
		)
	);
}
