<?php
/**
 * SVG Sprite Output.
 */

$classes = $args['classes'] ?? '';
$icon    = $args['icon'] ?? '';
?>

<svg class="o-svg o-svg--<?php echo esc_attr( $icon ); ?> <?php echo esc_attr( $classes ); ?>">
	<use class="o-svg__use " xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/symbol/svg/sprite.symbol.svg#<?php echo esc_attr( $icon ); ?>"/>
</svg>
