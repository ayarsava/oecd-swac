<?php
if ( ! empty( $args['block'] ) ) {
	$block        = $args['block'];
	$classes      = $args['classes'] ?? '';
	$count        = $args['count'];
	$pid          = get_the_ID();
	$content_type = get_post_type_label( $pid );

	if ( ! empty( $args['modifier'] ) ) {
		$classes .= ' c-text-block--' . $args['modifier'];
	}

	if ( ! empty( $block['wysiwyg_editor'] ) ) {
		?>
		<section
				class="c-text-block c-text-block--inline <?php echo esc_attr( $classes ); ?> o-section o-section--no-background">
			<div class="c-text-block__inner o-content-from-editor js-content-from-editor">
				<?php
				if ( ! empty( $block['title'] ) ) {
					?>
					<h2><?php echo esc_html( $block['title'] ); ?></h2>
					<?php
				}
				?>
				<?php
				if ( ! empty( $block['subtitle'] ) ) {
					?>
					<h3><?php echo esc_html( $block['subtitle'] ); ?></h3>
					<?php
				}
				?>
				<?php echo wp_kses( $block['wysiwyg_editor'], 'post' ); ?>
			</div>
		</section>
		<?php
	}
}
