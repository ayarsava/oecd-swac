<?php
/**
 * Call to action
 */
if (!empty($args['block'])) {
    $block = $args['block'];

    if (!empty($block['title']) || !empty($block['button'])) {
        $pid = get_the_ID();
        $mode = $block['mode'];
        $cta_title = $block['title'];
        $description = $block['description'];
        $button = $block['button'];
        $button_target = $button['target'] ? $button['target'] : '_self';
        $image = $block['image'];
        ?>
        <section
                class="c-call-to-action c-call-to-action--<?php echo esc_html($mode); ?> o-section" data-aos="fade-up"
                data-aos-duration="500">
            <div class="c-call-to-action__inner o-container">
                <div class="c-call-to-action__content">
                    <h2 class="c-call-to-action__title">
                        <?php echo wp_kses($cta_title, 'post'); ?>
                    </h2>

                    <?php
                    if (!empty($description)) {
                        ?>
                        <p class="c-call-to-action__description"><?php echo esc_html($description); ?></p>
                        <?php
                    }
                    ?>
                    <?php
                    if (!empty($button['url'])) {
                        ?>
                        <a class="c-call-to-action__button" href="<?php echo esc_url($button['url']); ?>"
                           target="<?php echo esc_attr($button_target); ?>"
                           title="<?php echo esc_attr($button['title']); ?>">
                            <?php
                            echo esc_html($button['title']);
                            ?>
                        </a>
                        <?php
                    }
                    ?>
                </div>

                <div class="c-call-to-action__image-wrapper">
                    <?php
                    if (!empty($image['ID'])) {
                        // Get this attachment ID
                        $attachment_id = $image['ID'];
                        $image_large_src = wp_get_attachment_image_src($attachment_id, 'full-width-retina');
                        ?>
                        <picture class="c-call-to-action__image">
                            <!-- Large art direction resolutions -->
                            <source media="(min-width: 50rem)"
                                    srcset="<?php echo wp_get_attachment_image_srcset($attachment_id, 'full-width'); ?>"
                                    sizes="(min-width: 60rem) 60rem, 100vw">

                            <!-- Small art direction resolutions -->
                            <source
                                    srcset="<?php echo wp_get_attachment_image_srcset($attachment_id, 'profile-image'); ?>"
                                    sizes="100vw">

                            <!-- fallback -->
                            <img src="<?php echo $image_large_src[0]; ?>"
                                 srcset="<?php echo wp_get_attachment_image_srcset($image_large_src, 'full-width'); ?>"
                                 sizes="(min-width: 60rem) 60rem, 100vw"
                                 alt="<?php get_post_meta($attachment_id, '_wp_attachment_image_alt', true) ?>"
                            >
                        </picture>
                        <?php

                    }
                    ?>
                </div>
            </div>
        </section>
        <?php
    }
}
