<?php

if (!empty($args['block'])) {
    $block = $args['block'];
    $extra_args = $args['extra_args'] ?? array();

    // Settings.


    $number_of_items = $block['number_of_items_to_show'];

    if ($number_of_items == null) {
        $number_of_items = '3';
    }

    $auto_enabled = $block['automatic_content_selection_enabled'];

    // Layout.
    $layout = $block['layout'];

    if ('three' === $layout || 'two' === $layout) {
        $disable_images = $block['disable_images'] ?? false;
        $title_position = $block['title_position'] ?? null;
    } else {
        $disable_images = false;
        $title_position = null;
    }
    // Manual Content.
    $manual_content = $block['manual_featured_content'];

    // Auto Content.
    $content_types_to_show = $block['content_types_to_show'];

    // Prep variables for featured content.

    if (empty($manual_content)) {
        $manual_content = array();
    }

    if (empty($content_types_to_show)) {
        $content_types_to_show = array();
    } else {
        $featured_content = get_featured_content($manual_content, $auto_enabled, $content_types_to_show, $number_of_items, $extra_args);
    }

    if ('true' !== $auto_enabled) {
        $featured_content = get_featured_content($manual_content, $auto_enabled, $content_types_to_show, $number_of_items, $extra_args);
    }


    if (is_array($featured_content) && !empty($featured_content)) {
        get_template_part(
            'assets/views/card-block',
            null,
            array(
                'title' => $block['title'],
                'items' => $featured_content,
                'layout' => $layout,
                'disable-images' => $disable_images,
                'title_position' => $title_position,
            )
        );
    }
}
