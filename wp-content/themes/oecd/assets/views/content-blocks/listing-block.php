<?php
/**
 * Listing block
 */

if (!empty($args['block'])) {
    $block = $args['block'];

    if (!empty($block['title']) || !empty($block['button'])) {
        $pid = get_the_ID();
        $post_types_to_list = $block['post_types_to_list'];
        $current_url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        ?>
        <section class="c-list js-filters">
            <div class="c-list__container o-container">
                <div class="c-list__filters">
                    <?php
                    if (empty($_GET['key'])) {
                        ?>
                        <div class="c-list__filters-topics" id="js-get-url">
                            <?php
                            $args = array(
                                'post_type' => $post_types_to_list,
                                'posts_per_page' => - 1,
                                'post_status' => 'publish'
                            );

                            $the_query = new WP_Query($args);
                            $my_terms = array();
                            if ($the_query->have_posts()) {
                                while ($the_query->have_posts()) {
                                    $the_query->the_post();

                                    $terms = get_the_terms(get_the_ID(), 'topics');
                                    if ($terms && !is_wp_error($terms)) :
                                        foreach ($terms as $term) {
                                            if (!in_array($term->term_id, $my_terms))
                                                $my_terms[] = $term->term_id;
                                        }
                                    endif;
                                }
                                wp_reset_postdata();
                            }

                            $arr_params = array('key', 'term');
                            echo '<a class="c-list__filters-btn o-button" href="' . esc_url(remove_query_arg($arr_params)) . '">All</a>';

                            if (sizeof($my_terms)) {
                                usort($my_terms, 'sortByName');
                                foreach ($my_terms as $term_id) {
                                    $category = get_term_by('id', $term_id, 'topics');
                                    echo '<a class="c-list__filters-btn o-button" href="' . esc_url(add_query_arg('term', $category->slug)) . '">' . $category->name . '</a>';
                                }
                            }

                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="c-list__search-wrapper">
                        <?php
                        if (!empty($_GET['key'])) {
                            ?>
                            <button id="keyremover" class="c-list__key-remover o-button key-remover">
                                <?php
                                echo esc_attr($_GET['key']);

                                get_template_part(
                                    'assets/views/svg',
                                    null,
                                    array(
                                        'classes' => 'c-list__cross-icon',
                                        'icon' => 'cross',
                                    )
                                );
                                ?>
                            </button>
                            <?php
                        } else {
                            ?>
                            <form class="c-search-form--header" action="<?php $current_url; ?>"
                                  method="get" role="search">
                                <input class="c-search-form__input" id="search-input" name="key" type="text"
                                       placeholder="Search"/>
                                <button class="c-search-form__button" title="Search" type="submit" aria-label="Search">
                                    <?php
                                    get_template_part(
                                        'assets/views/svg',
                                        null,
                                        array(
                                            'classes' => 'c-search-form__icon',
                                            'icon' => 'search',
                                        )
                                    );
                                    ?>
                                </button>
                            </form>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <?php
                $posts_per_page = 8;
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;


                $queried_terms = get_query_var('term');
                if (!empty($queried_terms)) {
                    $operator = 'IN';
                } else {
                    $operator = 'NO';
                }
                $args = array(
                    'post_type' => $post_types_to_list,
                    'post_status' => 'publish',
                    'posts_per_page' => $posts_per_page,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'paged' => $paged,

                    'tax_query' => [
                        [
                            'taxonomy' => 'topics',
                            'field' => 'slug',
                            'terms' => $queried_terms,
                            'operator' => $operator
                        ],
                    ],
                );

                if (isset($_GET['key'])) {
                    $args = array(
                        's' => $_GET['key'],
                    );
                }

                $query = new WP_Query($args);

                if ($query->have_posts()) :
                    ?>
                    <div class="c-card-block c-card-block--two c-card-block--<?php echo esc_attr($post_types_to_list); ?>">
                        <div class="c-card-block__cards-wrapper">
                            <?php
                            while ($query->have_posts()): $query->the_post();
                                get_template_part('assets/views/card', null, array('pid' => $query->post->ID));
                            endwhile;
                            ?>
                        </div>
                    </div>
                    <?php
                    wp_reset_postdata();
                else :
                    echo '<div class="c-list__no-results">There are no posts for this specific search</div>';
                endif;

                echo '<nav class="c-list__pagination o-section">';
                pagination_bar($query);
                echo '</nav>';
                ?>
            </div>
        </section>
        <?php
    }
}
?>
