<?php
if ( ! empty( $args['block'] ) ) {
	$block = $args['block'];

	if ( ! empty( $block['embed_url'] ) ) {
		$embed_code = $block['embed_url'];
		// $embed_width = $block['width'];
		$embed_height = $block['height'] ?? '';
		$button       = $block['button'];
		if ( ! empty( $button ) ) {
			$button_target = $button['target'] ? $button['target'] : '_self';
		}
		?>
		<section class="c-embed o-section">
			<div class="c-embed__container">
				<div class="c-embed__inner" id="data-viz">
					<?php
					if ( empty( $embed_height ) ) {
						echo '<div class="c-embed__iframe"
					data-pym-src="' . esc_url( $embed_code ) . '"
					data-pym-allowfullscreen></div>';
					} else {
						?>
						<iframe width="100%"
								height="<?php echo $embed_height; ?>"
								src="<?php echo esc_url( $embed_code ) ?>"></iframe>
						<?php
					}
					?>
				</div>
				<?php
				if ( ! empty( $button ) ) {
					?>
					<div class="c-embed__button-wrapper c-embed__container">
						<button class="c-embed__button o-button c-embed__generator-button"
								href="<?php echo esc_url( $button['url'] ); ?>"
								target="<?php echo esc_attr( $button_target ); ?>"
								title="<?php echo esc_attr( $button['title'] ); ?>">
							Download Image
						</button>


						<a class="c-embed__button o-button" href="<?php echo esc_url( $button['url'] ); ?>"
						   target="<?php echo esc_attr( $button_target ); ?>"
						   title="<?php echo esc_attr( $button['title'] ); ?>">
							<?php
							echo esc_html( $button['title'] );
							?>
						</a>
					</div>
					<?php
				}
				?>
			</div>
		</section>
		<?php
	}
}
