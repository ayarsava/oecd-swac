<?php
/**
 * Logo carousel
 */

if ( ! empty( $args['block'] ) ) {
	$block = $args['block'];

	$section_title = $block['title'];
	$subtitle      = $block['subtitle'];
	$description   = $block['description'];
	$items         = $block['logos'];
	if ( $items ) {
		?>
		<div class="c-logo-block">

			<div class="c-logo-block__container o-container">
				<?php
				if ( ! empty( $section_title ) ) {
					?>
					<h3 class="c-logos__title"><?php echo esc_html( $section_title ); ?></h3>
					<?php
				}
				?>
				<?php
				foreach ( $items as $item ) {
					$item_title = $item['logo_title'];
					$item_image = $item['logo_image'];
					$item_url   = $item['logo_url'];
					?>
					<div class="c-logos c-logos__item">
						<?php
						if ( ! empty( $item_title ) ) {
							?>
							<h3><?php echo esc_html( $item_title ); ?></h3>
							<?php
						}
						?>


						<?php
						if ( $item_url ) {
							echo esc_url( $item_url );
						}
						?>

						<?php
						if ( ! empty( $item_image['ID'] ) ) {
							// Get this attachment ID
							$item_image_id   = $item_image['ID'];
							$image_large_src = wp_get_attachment_image_src( $item_image_id, 'full-width-retina' );
							?>

							<picture class="c-call-to-action__image">
								<!-- Large art direction resolutions -->
								<source media="(min-width: 50rem)"
										srcset="<?php echo wp_get_attachment_image_srcset( $item_image_id, 'full-width' ); ?>"
										sizes="(min-width: 60rem) 60rem, 100vw">

								<!-- Small art direction resolutions -->
								<source
										srcset="<?php echo wp_get_attachment_image_srcset( $item_image_id, 'profile-image' ); ?>"
										sizes="100vw">

								<!-- fallback -->
								<img src="<?php echo $image_large_src[0]; ?>"
									 srcset="<?php echo wp_get_attachment_image_srcset( $image_large_src, 'full-width' ); ?>"
									 sizes="(min-width: 60rem) 60rem, 100vw"
									 alt="<?php get_post_meta( $item_image_id, '_wp_attachment_image_alt', true ) ?>"
								>
							</picture>
							<?php

						}
						?>
					</div>
					<?php
				}
				?>


				<?php
				if ( ! empty( $subtitle ) ) {
					?>
					<p class="c-logos__subtitle"><?php echo esc_html( $subtitle ); ?></p>
					<?php
				}
				?>
				<?php
				if ( ! empty( $description ) ) {
					?>
					<p class="c-logos__description"><?php echo esc_html( $description ); ?></p>
					<?php
				}
				?>
			</div>
		</div>
		<?php
	}
	?>
	<?php
}
