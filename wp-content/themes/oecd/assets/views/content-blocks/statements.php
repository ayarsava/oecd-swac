<?php
/**
 * Statements
 */

if ( ! empty( $args['block'] ) ) {
	$block = $args['block'];

	$statements = $block['statements'];
	if ( $statements ) {
		?>
		<div class="c-statements">
			<div class="c-statements__container">
				<div class="c-statements__items">
					<?php
					foreach ( $statements as $statement ) {
						$statement_title       = $statement['statement_title'];
						$statement_description = $statement['statement_text'];
						?>
						<div class="c-statements__item">
							<?php
							if ( ! empty( $statement_title ) ) {
								?>
								<h3 class="c-statements__title"><?php echo esc_html( $statement_title ); ?></h3>
								<?php
							}
							?>
							<div class="c-statements__description o-content-from-editor">
								<?php echo wp_kses( $statement_description, 'post' ); ?>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	<?php
}
