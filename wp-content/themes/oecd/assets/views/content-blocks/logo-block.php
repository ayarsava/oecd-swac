<?php
/**
 * Logo block
 */

if (!empty($args['block'])) {
    $block = $args['block'];

    $block_title = $block['title'];
    $subtitle = $block['subtitle'];
    $description = $block['description'];
    $items = $block['logos'];

    ?>

    <section class="c-logo-block o-section">
        <div class="c-logo-block__container">
            <?php
            if (!empty($block_title)) {
                ?>
                <h2 class="c-logo-block__title">
                    <?php echo esc_html($block_title); ?>
                </h2>
                <?php
            }
            ?>

            <div class="c-logo-block__wrapper">
                <?php
                foreach ($items as $item) {
                    $item_title = $item['logo_title'];
                    $item_image = $item['logo_image'];
                    $item_url = $item['logo_url'];
                    ?>
                    <div class="c-logo-block__item">
                        <?php
                        if ($item_url) {
                        ?>
                        <a href="<?php echo esc_url($item_url); ?>">
                            <?php
                            }
                            ?>

                            <?php
                            if (!empty($item_image['ID'])) {
                                // Get this attachment ID
                                $item_image_id = $item_image['ID'];
                                $image_large_src = wp_get_attachment_image_src($item_image_id, 'full-width-retina');
                                ?>

                                <picture class="c-logo-block__image">
                                    <!-- Large art direction resolutions -->
                                    <source media="(min-width: 50rem)"
                                            srcset="<?php echo wp_get_attachment_image_srcset($item_image_id, 'full-width'); ?>"
                                            sizes="(min-width: 60rem) 60rem, 100vw">

                                    <!-- Small art direction resolutions -->
                                    <source
                                            srcset="<?php echo wp_get_attachment_image_srcset($item_image_id, 'full-width'); ?>"
                                            sizes="100vw">

                                    <!-- fallback -->
                                    <img src="<?php echo $image_large_src[0]; ?>"
                                         srcset="<?php echo wp_get_attachment_image_srcset($image_large_src, 'full-width'); ?>"
                                         sizes="(min-width: 60rem) 60rem, 100vw"
                                         alt="<?php echo esc_html($item_title); ?>"
                                    >
                                </picture>
                                <?php

                            }
                            ?>

                            <?php
                            if ($item_url) {
                            ?>
                        </a>
                    <?php
                    }
                    ?>
                    </div>
                    <?php
                }
                ?>
            </div>

            <?php
            if (!empty($subtitle)) {
                ?>
                <p class="c-logo-block__subtitle">
                    <?php echo esc_html($subtitle); ?>
                </p>
                <?php
            }

            if (!empty($description)) {
                ?>
                <p class="c-logo-block__description">
                    <?php echo esc_html($description); ?>
                </p>
                <?php
            }
            ?>
        </div>

    </section>
    <?php
}
