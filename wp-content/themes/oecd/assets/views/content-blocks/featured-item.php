<?php

if ( ! empty( $args['block'] ) ) {
	$block = $args['block'];

	if ( ! empty( $block['featured_item'] ) ) {
		get_template_part(
			'assets/views/card-block',
			null,
			array(
				'layout'      => 'one',
				'title'       => $block['title'],
				'description' => $block['description'],
				'items'       => array( $block['featured_item'] ),
			)
		);
	}
}
