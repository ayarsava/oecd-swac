<?php
/**
 * The header of the whole website.
 */


$pid = get_the_ID();

// Mode
$mode = get_field('header_mode', $pid);
?>

<a class="c-skip-button" href="#content" title="Skip to content">
    Skip to content
</a>

<header class="c-header js-header<?php if (!empty ($mode)) {
    echo ' c-header--' . $mode;
} ?>">

    <div class="c-header__container o-container">
        <div class="c-header__logo-block">
            <a class="c-header__logo" href="<?php echo esc_url(home_url()); ?>" title="Go to homepage">

                <?php
                if ('en' == pll_current_language()) {
                    if ('dark' === $mode) {
                        ?>
                        <img class="c-header__logo-type"
                             src="<?php echo get_template_directory_uri(); ?>/assets/img/swac-oecd-white.png">
                    <?php } else { ?>
                        <img class="c-header__logo-type"
                             src="<?php echo get_template_directory_uri(); ?>/assets/img/swac-oecd.png">
                        <?php
                    }

                } else {
                    if ('dark' === $mode) {
                        ?>
                        <img class="c-header__logo-type"
                             src="<?php echo get_template_directory_uri(); ?>/assets/img/logocsao-ocde-white.png">
                    <?php } else { ?>
                        <img class="c-header__logo-type"
                             src="<?php echo get_template_directory_uri(); ?>/assets/img/logocsao-ocde.png">
                        <?php
                    }
                }
                ?>
            </a>
            <button class="c-header__burger js-burger-button" aria-label="Open the menu">
                MENU
            </button>
        </div>

        <div class="c-header__contents js-burger-content">
            <div class="c-header__menu-block">
                <div class="c-header__menu-wrapper js-main-menu">
                    <nav class="c-main-menu<?php if (!empty ($mode)) {
                        echo ' c-main-menu--' . $mode;
                    } ?>">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                                'menu_id' => 'main-menu',
                                'menu_class' => 'c-main-menu__list c-header__menu-wrapper',
                            )
                        );
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<main id="content" class="js-content">
