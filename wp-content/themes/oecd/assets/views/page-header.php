<?php
/**
 * Page Header
 */
$pid = get_the_ID();

// Mode
$mode = get_field('header_mode', $pid);


// Title.
if (!isset($args['title'])) {
    $header_title = get_the_title($pid);
} else {
    $header_title = $args['title'];
}

// Subtitle.
if (!isset($args['subtitle'])) {
    $subtitle = get_field('subtitle', $pid);
} else {
    $subtitle = $args['subtitle'];
}

// Excerpt.
if (!isset($args['description'])) {
    $description = get_field('description', $pid);
} else {
    $description = $args['description'];
}

?>
<header class="c-page-header c-page-header--<?php echo esc_attr($mode); ?><?php if (!empty(get_page_template_slug())) {
    echo ' c-page-header--' . esc_html(str_replace(".php", "", get_page_template_slug()));
}
if (empty(has_post_thumbnail())) {
    echo ' c-page-header--no-thumbnail';
} ?>">
    <div class="c-page-header__container o-container">
        <div class="c-page-header__inner">
            <div class="c-page-header__heading">
                <h1 class="c-page-header__title">
                    <?php echo esc_html($header_title); ?>
                </h1>

                <?php
                if (!empty($subtitle)) {
                    ?>
                    <div class="c-page-header__subtitle"><?php echo esc_html($subtitle); ?></div>
                    <?php
                }
                ?>

                <?php
                if (!empty($description)) {
                    ?>
                    <p class="c-page-header__subtitle"><?php echo esc_html($description); ?></p>
                    <?php
                }
                ?>

                <?php
                $buttons_type = get_field('buttons_type');
                $buttons = get_field('buttons');
                if ($buttons) {
                    ?>
                    <div class="c-card-block__button-wrapper c-card-block__button-wrapper--<?php echo $buttons_type;
                    if ($buttons_type == 'tab') {
                        echo ' c-tabs';
                    } ?>"
                        <?php if ($buttons_type == 'tab') {
                            echo 'id="js-get-url"';
                        } ?>>
                        <?php
                        foreach ($buttons as $button) {
                            $button = $button['button'];
                            ?>
                            <a class="c-card-block__button o-button"
                               href="<?php echo esc_url($button['url']); ?>"
                               target="<?php echo esc_attr($button['target']); ?>"
                               title="<?php echo esc_attr($button['title']); ?>">
                                <?php
                                echo esc_html($button['title']);
                                ?>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>


            <?php
            if (has_post_thumbnail()) {
                ?>
                <div class="c-page-header__side-image-wrapper">
                    <?php
                    the_post_thumbnail('full-width-retina', array('class' => 'c-page-header__side-image'));
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
</header>
