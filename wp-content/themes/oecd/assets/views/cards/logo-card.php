<?php
/**
 * Logo card
 */


$logo_image = get_sub_field( 'logo_image' );

if ( ! empty( $args['pid'] ) ) {
	$pid        = $args['pid'];
	$logo       = get_field( 'featured_image', $pid );
	$url        = get_the_permalink( $pid );
	$logo_title = get_the_title( $pid );
	?>
	<a class="c-logo-card" href="<?php echo esc_url( $url ); ?>" title="<?php echo esc_html( $logo_title ); ?>">
		<?php
		if ( ! empty( $logo ) ) {
			echo '<div class="c-logo-card__container">';


			echo get_the_attachment_picture(
				'logo_image',
				array(
					'(min-width:1px)'    => 'logo_image',
					'(min-width:500px)'  => 'logo_image',
					'(min-width:768px)'  => 'logo_image',
					'(min-width:1018px)' => 'full-width',
					'(min-width:1280px)' => 'full-width-retina',
				),
				$logo['id'],
				array(
					'class' => 'c-logo-card__image',
				)
			);


			echo '</div>';
		} else {
			echo esc_html( $logo_title );
		}
		?>
	</a>

	<?php
}
