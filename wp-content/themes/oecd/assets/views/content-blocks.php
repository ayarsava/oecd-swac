<?php
$pid        = $args['pid'] ?? get_the_ID();
$components = get_field( 'components', $pid );

if ( ! empty( $components ) ) {
	foreach ( $components as $block ) {
		switch ( $block['acf_fc_layout'] ) {
			case 'call_to_action':
				$template = 'assets/views/content-blocks/call-to-action';
				break;

			case 'featured_content':
				$template = 'assets/views/content-blocks/featured-content';
				break;

			case 'map_block':
				$template = 'assets/views/content-blocks/map-block';
				break;

			case 'listing_block':
				$template = 'assets/views/content-blocks/listing-block';
				break;

			case 'logos_block':
				$template = 'assets/views/content-blocks/logo-block';
				break;

			case 'people':
				$template = 'assets/views/content-blocks/people-block';
				break;

			case 'statements':
				$template = 'assets/views/content-blocks/statements';
				break;

			case 'text_block':
				$template = 'assets/views/content-blocks/text-block';
				break;

			case 'topics_repeater':
				$template = 'assets/views/content-blocks/topics';
				break;

			default:
				// Component doesn't exist, move to the next one.
				$template = null;
				continue 2;
		}

		$args = array(
			'block' => $block,
			'pid'   => $pid,
		);

		if ( isset( $template ) && ! empty( $template ) ) {
			get_template_part( $template, null, $args );
		}
	}
}
