<?php
/**
 * Single header - called at single.php
 */
?>
<?php
$pid         = get_the_ID();
$description = get_field( 'description', $pid );
$tax_terms   = get_the_terms( $pid, 'topics' );
?>
<header class="c-single-header">
	<!--<div class="c-single-header__type o-container">
		<?php
	foreach ( $tax_terms as $tax_term ) { ?>
			<span><a href="<?php echo esc_url( get_term_link( $tax_term ) ) ?>">
			<?php echo esc_html( $tax_term->name ); ?>
		</a></span>
			<?php
	}
	?>
	</div>-->

	<div class="c-single-header__container o-container">
		<div class="c-single-header__inner">
			<div class="c-single-header__heading">
				<h1 class="c-single-header__title">
					<?php
					if ( ! isset( $args['title'] ) ) {
						the_title();
					} else {
						echo esc_html( $args['title'] );
					}
					?>
				</h1>

				<?php
				if ( ! empty( $description ) ) {
					?>
					<p class="c-single-header__description"><?php echo esc_html( $description ); ?></p>
					<?php
				}
				?>

			</div>

			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail( 'thumbnail', array( 'class' => 'c-single-header__side-image' ) );
			}
			?>
		</div>
	</div>
</header>
