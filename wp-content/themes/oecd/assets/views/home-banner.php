<?php
/**
 * Homepage banner
 */

$pid = get_the_ID();
$hero_title = get_field('title', $pid);
$description = get_field('description', $pid);
$button = get_field('hero_button', $pid);
?>
<header class="c-home-banner">
    <div class="c-home-banner__container">
        <div class="c-home-banner__inner">
            <div class="c-home-banner__heading">
                <div class="c-home-banner__title">
                    <?php echo wp_kses($hero_title, 'post'); ?>
                </div>
                <div class="c-home-banner__text" data-aos="fade-up" data-aos-duration="500">
                    <?php
                    if (!empty($description)) {
                        ?>
                        <div class="c-home-banner__description">
                            <?php echo wp_kses($description, 'post'); ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if (!empty($button['url'])) {
                        ?>
                        <a class="c-home-banner__button o-button" href="<?php echo esc_url($button['url']); ?>"
                           target="<?php echo esc_attr($button['target']); ?>">
                            <?php
                            echo esc_html($button['title']);
                            get_template_part('assets/views/svg', null, array('icon' => 'arrow'));
                            ?>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="c-home-banner__image-wrapper">
                <?php
                if (has_post_thumbnail()) {
                    ?>
                    <?php
                    the_post_thumbnail('full-width-retina', array('class' => 'c-side-image'));
                    ?>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</header>