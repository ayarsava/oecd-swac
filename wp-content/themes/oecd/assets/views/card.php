<?php
/**
 * Card component.
 */

if (!empty($args['pid'])) {
    $pid = $args['pid'];
    $card_title = get_the_title($pid);
    $card_type = get_post_type_label($pid);
    $card_date = get_post_date($pid);
    $card_url = get_the_permalink($pid);
    $card_description = get_field('description', $pid);
    $card_description_text = substr($card_description, 0, strpos($card_description, "."));
    $external_url = get_field('external_url', $pid);
    $tax_terms = get_the_terms($pid, 'topics');
    $tax_types = get_the_terms($pid, 'types');
    ?>
    <div class="c-card" data-aos="c-card-animation" data-aos-duration="300">
        <?php
        if ('report' == get_post_type() && is_page_template('tpl-listing.php') && has_post_thumbnail()) {
            ?>
            <div class="c-card__side-image-wrapper">
                <?php
                the_post_thumbnail('thumbnail', array('class' => 'c-card__side-image'));
                ?>
            </div>
            <?php
        }
        ?>

        <?php
        if ('post' == get_post_type() && !empty($tax_types)) {
            ?>
            <div class="c-card__terms--types">
                <?php
                foreach ($tax_types as $tax_type) { ?>
                    <span class="c-card__term c-card__term-<?php echo esc_html($tax_type->slug); ?>">
						<?php echo esc_html($tax_type->name); ?>
					</span>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>


        <?php
        if ($external_url) {
            ?>
            <a href="<?php echo esc_url($external_url); ?>"
               target="_blank"><h3 class="c-card__title"><?php echo esc_html($card_title); ?>
                </h3></a>
            <?php
        } else {
            ?>
            <a href="<?php echo esc_url($card_url); ?>"><h3
                        class="c-card__title"><?php echo esc_html($card_title); ?>
                </h3></a>
            <?php
        }
        ?>
        <div class="c-card__body">
            <?php echo wp_kses($card_description_text, 'post'); ?>
        </div>

        <div class="c-card__footer">
            <a class="c-card__link" href="<?php if ($external_url) {
                echo esc_url($external_url);
            } else {
                echo esc_url($card_url);
            } ?>"<?php if ($external_url) {
                echo ' target="_blank"';
            } ?>>
                <?php
                if ('en' == pll_current_language()) {
                    echo 'Read more';
                } else {
                    echo 'Plus';
                } ?>
            </a>
        </div>

        <?php
        if ('post' == get_post_type() && !empty($tax_terms)) {
            ?>
            <div class="c-card__terms">
                <?php
                foreach ($tax_terms as $tax_term) { ?>
                    <span class="c-card__term">
						<!--<a class="c-card__term" href="<?php echo esc_url(get_term_link($tax_term)) ?>">-->
						<?php echo esc_html($tax_term->name); ?>
					<!--</a>-->
					</span>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
