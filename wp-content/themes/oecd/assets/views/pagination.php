<?php
$found_posts    = $args['found_posts'];
$posts_per_page = $args['posts_per_page'];

if ( isset( $found_posts ) && isset( $posts_per_page ) && - 1 !== $posts_per_page ) {
	$pagination_get_key   = 'current-page';
	$pagination_get_value = filter_input( INPUT_GET, $pagination_get_key, FILTER_SANITIZE_NUMBER_INT );

	if ( 0 !== $found_posts && $found_posts > $posts_per_page ) {
		$class     = '';
		$min_page  = 1;
		$num_pages = ceil( $found_posts / $posts_per_page );
		$url       = site_url();

		if ( isset( $_SERVER['REQUEST_URI'] ) ) {
			$url = site_url() . filter_var( wp_unslash( $_SERVER['REQUEST_URI'] ), FILTER_SANITIZE_URL );
		}

		// Settings.
		$show_prev      = false;
		$show_prev_dots = false;

		$show_next      = false;
		$show_next_dots = false;

		$show_first = true;
		$show_last  = true;

		// Get $current_page variable.
		if ( empty( $current_page ) && ! empty( $pagination_get_value ) ) {
			$current_page = intval( $pagination_get_value );

		} elseif ( empty( $current_page ) ) {
			$current_page = 1;
			$min_page     = 1;
		}

		if ( $current_page <= 3 ) {
			if ( $num_pages <= 5 ) {
				$max_page = $num_pages;
			} else {
				$max_page = 5;
			}
		} elseif ( $current_page != $num_pages ) {
			$min_page = $current_page - 2;
			$max_page = ( $current_page + 2 ) <= $num_pages ? $current_page + 2 : $num_pages;
		} else {
			$max_page = $num_pages;
		}

		// If less than 5 pages, minP is always 1.
		if ( $num_pages <= 5 ) {
			$min_page = 1;
		}

		// If current page is second to last page, show three prior pages.
		if ( $current_page + 1 == $num_pages && $num_pages >= 5 ) {
			$min_page = $current_page - 3;
		}

		// If current page is last page, show four prior pages and don't show last button.
		if ( $current_page == $num_pages && $num_pages >= 5 ) {
			$min_page  = $current_page - 4;
			$show_last = false;
		}

		// If current page is first page, don't show first button.
		if ( $current_page == 1 ) {
			$show_first = false;
		}

		// Previous Dots.
		if ( $min_page > 1 ) {
			$show_prev_dots = true;
		}

		// Previous Button.
		if ( $num_pages > $max_page ) {
			$show_next_dots = true;
		}

		// Next Dots.
		if ( $current_page > 1 ) {
			$show_prev = true;
		}

		// Next Button.
		if ( $current_page <= $num_pages - 1 ) {
			$show_next = true;
		}

		// Open List.
		$buttons = array();

		// Icons
		// Next page icon.
		$arrow_svg = load_template_part(
			'assets/views/svg',
			null,
			array(
				'classes' => 'c-pagination__icon',
				'icon'    => 'next-page',
			)
		);

		// Last page icon.
		$end_arrow_svg = load_template_part(
			'assets/views/svg',
			null,
			array(
				'classes' => 'c-pagination__icon',
				'icon'    => 'last-page',
			)
		);

		// Jump to First Page Button.
		if ( $show_first ) {
			$url       = add_query_arg( $pagination_get_key, '1', $url );
			$buttons[] = array(
				'aria-label' => 'Go to first page',
				'modifier'   => 'first',
				'label'      => 'First',
				'url'        => $url,
			);
		}

		// Show Previous Button.
		if ( true === $show_prev ) {
			$page_to_show = $current_page - 1;
			$url          = add_query_arg( $pagination_get_key, $page_to_show, $url );
			$buttons[]    = array(
				'aria-label' => 'Go to previous page',
				'modifier'   => 'previous',
				'label'      => $arrow_svg,
				'url'        => $url,
			);
		}

		if ( true === $show_prev_dots ) {
			// Next Page on from current page.
			$page_to_show = $min_page - 1;
			$url          = add_query_arg( $pagination_get_key, $page_to_show, $url );
			$buttons[]    = array(
				'aria-label' => 'Go to page ' . ( $min_page + 1 ),
				'modifier'   => 'dotted',
				'label'      => ' ... ',
				'url'        => $url,
			);
		}

		// Generate number links.
		for ( $page_number = $min_page; $page_number <= $max_page; $page_number ++ ) {
			$url = add_query_arg( $pagination_get_key, $page_number, $url );

			if ( intval( $current_page ) === intval( $page_number ) ) {
				$class = 'is-current';
			} else {
				$class = '';
			}

			$buttons[] = array(
				'aria-label' => 'Go to page ' . $page_number,
				'class'      => $class,
				'modifier'   => 'number',
				'label'      => $page_number,
				'url'        => $url,
			);
		}

		if ( true === $show_next_dots ) {
			// Next Page on from current page.
			$page_to_show = $max_page + 1;
			$url          = add_query_arg( $pagination_get_key, $page_to_show, $url );
			$buttons[]    = array(
				'aria-label' => 'Go to page ' . ( $max_page + 1 ),
				'modifier'   => 'dotted',
				'label'      => ' ... ',
				'url'        => $url,
			);
		}

		// Show Next button.
		if ( true === $show_next ) {
			// Next Page on from current page.
			$page_to_show = $current_page + 1;
			$url          = add_query_arg( $pagination_get_key, $page_to_show, $url );
			$buttons[]    = array(
				'aria-label' => 'Go to next page',
				'modifier'   => 'next',
				'label'      => $arrow_svg,
				'url'        => $url,
			);
		}

		// Jump to last page button.
		if ( $show_last ) {
			$url       = add_query_arg( $pagination_get_key, $num_pages, $url );
			$buttons[] = array(
				'aria-label' => 'Go to last page',
				'modifier'   => 'last',
				'label'      => 'Last',
				'url'        => $url,
			);
		}

		if ( $num_pages >= 5 ) {
			$class = 'c-pagination--has-five';
		}
		?>
		<nav class="c-pagination <?php echo esc_attr( $class ); ?>">
			<?php
			if ( is_array( $buttons ) ) {
				foreach ( $buttons as $button ) {
					if ( empty( $button['class'] ) ) {
						$button['class'] = '';
					}
					?>
					<a class="c-pagination__button c-pagination__button--<?php echo esc_attr( $button['modifier'] . ' ' . $button['class'] ); ?>"
					   href="<?php echo esc_url( $button['url'] ); ?>#listing"
						<?php if ( ! empty( $button['aria-label'] ) ) { ?>
							aria-label="<?php echo esc_attr( $button['aria-label'] ); ?>"
						<?php } ?>
					>
						<?php echo $button['label']; ?>
					</a>
					<?php
				}
			}
			?>
		</nav>
		<?php
	}
}
