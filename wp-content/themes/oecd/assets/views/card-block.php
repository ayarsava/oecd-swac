<?php
if (!empty($args['items'])) {
    $block_title = $args['title'] ?? false;
    $description = $args['description'] ?? false;
    $items = $args['items'];
    $button = $args['button'] ?? false;
    $layout = $args['layout'] ?? 'two';
    if (count($items) === 3) {
        $layout = 'three';
    }

    if (empty($description)) {
        $class = 'c-card-block__title--bottom';
    }
    ?>
    <section class="c-card-block c-card-block--<?php echo esc_attr($layout); ?> o-section">
        <div class="c-card-block__container">
            <div class="o-container">
                <?php
                if (!empty($block_title)) {
                    ?>
                    <h2 class="c-card-block__title <?php echo esc_attr($class); ?>">
                        <?php echo esc_html($block_title); ?>
                    </h2>
                    <?php
                }

                if (!empty($description)) {
                    ?>
                    <p class="c-card-block__sub-title">
                        <?php echo esc_html($description); ?>
                    </p>
                    <?php
                }
                ?>

                <div class="c-card-block__cards-wrapper">
                    <?php
                    foreach ($items as $item_id) {
                        get_template_part('assets/views/card', null, array('pid' => $item_id));
                    }
                    ?>
                </div>


            </div>
        </div>
        <?php
        if (!empty($button)) {
            ?>
            <div class="c-card-block__button-wrapper o-container">
                <a class="c-card-block__button o-button" href="<?php echo esc_url($button['url']); ?>"
                   target="<?php echo esc_attr($button['target']); ?>"
                   title="<?php echo esc_attr($button['title']); ?>">
                    <?php
                    echo esc_html($button['title']);

                    get_template_part('assets/views/svg', null, array('icon' => 'arrow'));
                    ?>
                </a>
            </div>
            <?php
        } ?>
    </section>

    <?php
}
