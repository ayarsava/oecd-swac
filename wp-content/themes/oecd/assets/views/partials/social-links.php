<?php
/**
 * Social links to appear in the footer
 */

$twitter_handle = get_field( 'twitter_handle', 'option' );
$twitter_url    = '';

if ( ! empty( $twitter_handle ) ) {
	$twitter_url = 'https://twitter.com/' . $twitter_handle;
}

if ( ! isset( $extra_class ) ) {
	$extra_class = '';
}

$social_links = array(
	'facebook' => array(
		'alt_text' => 'Facebook',
		'class'    => 'facebook',
		'url'      => get_field( 'facebook_url', 'option' ),
	),
	'twitter'  => array(
		'alt_text' => 'Twitter',
		'class'    => 'twitter',
		'url'      => $twitter_url,
	),
	'linkedin' => array(
		'alt_text' => 'LinkedIn',
		'class'    => 'linkedin',
		'url'      => get_field( 'linkedin_url', 'option' ),
	),
);

$social_links_url = array_column( $social_links, 'url' );
if ( ! empty( $social_links_url ) ) {
	?>
	<div class="c-social-links <?php echo esc_attr( $extra_class ); ?>">
		<div class="c-social-links__title">swac oecd</div>
		<div class="c-social-links__icons">
			<?php
			foreach ( $social_links as $social ) {
				if ( ! empty( $social['url'] ) ) {
					?>
					<a class="c-social-links__link js-social-share" href="<?php echo esc_url( $social['url'] ); ?>"
					   target="_blank" title="<?php echo esc_html( $social['alt_text'] ); ?>">
						<?php
						get_template_part(
							'assets/views/svg',
							null,
							array(
								'classes' => 'c-social-links__icon c-social-links__icon--' . $social['class'],
								'icon'    => $social['class'],
							)
						);
						?>
					</a>
					<?php
				}
			}
			?>
		</div>
	</div>
	<?php
}
?>
