<div class="c-back-to-top o-container">
	<div class="c-back-to-top__button js-top">
		Back to top
		<?php
		get_template_part( 'assets/views/svg', null, array( 'icon' => 'arrow-down' ) );
		?>
	</div>
</div>
