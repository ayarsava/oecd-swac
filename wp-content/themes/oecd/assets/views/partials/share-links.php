<?php
/**
 * Generate the social media sharer buttons.
 * - Achieved without loading each platform's scripts and tracking.
 * - Based on https://jonsuh.com/blog/social-share-links/.
 */

$pid            = $args['post_id'] ?? get_the_ID();
$site_title     = get_bloginfo( 'name' );
$post_title     = get_the_title( $pid );
$post_link      = get_the_permalink( $pid );
$excerpt        = get_page_builder_excerpt( $pid, 180 );
$twitter_handle = get_field( 'twitter_handle', 'option' );

$twitter_share_link = '//twitter.com/share?' . http_build_query(
		array(
			'text' => $post_title,
			'url'  => $post_link,
			'via'  => ltrim( $twitter_handle, '@' ),
		)
	);

$linkedin_share_link = '//www.linkedin.com/shareArticle?' . http_build_query(
		array(
			'mini'    => 'true',
			'url'     => $post_link,
			'title'   => $post_title,
			'summary' => $excerpt,
			'source'  => $site_title,
		)
	);

$facebook_share_link = '//www.facebook.com/sharer/sharer.php?' . http_build_query(
		array(
			'u' => $post_link,
			't' => $post_title,
		)
	);

$links = array(
	'facebook' => array(
		'alt_text' => 'Facebook',
		'icon'     => 'facebook',
		'link'     => $facebook_share_link,
		'popup'    => true,
	),
	'twitter'  => array(
		'alt_text' => 'Twitter',
		'icon'     => 'twitter',
		'link'     => $twitter_share_link,
		'popup'    => true,
	),
	'linkedin' => array(
		'alt_text' => 'LinkedIn',
		'icon'     => 'linkedin',
		'link'     => $linkedin_share_link,
		'popup'    => true,
	),
);
?>

<div class="c-share">
	<h5 class="c-share__heading">Share this page</h5>

	<div class="c-share__links">
		<?php
		foreach ( $links as $k => $post_link ) {
			if ( $post_link['popup'] ) {
				$js_class = 'js-social-share';
				$target   = '_blank';
			} else {
				$js_class = '';
				$target   = '_self';
			}
			?>
			<a class="c-share__link <?php echo esc_attr( $js_class ); ?>" href="<?php echo esc_attr( $post_link['link'] ); ?>" target="<?php echo esc_attr( $target ); ?>" title="Share on <?php echo esc_attr( $post_link['alt_text'] ); ?>">
				<?php
				get_template_part(
					'assets/views/svg',
					null,
					array(
						'classes' => 'c-share__icon c-share__icon--' . $k,
						'icon'    => $post_link['icon'],
					)
				);
				?>
			</a>
			<?php
		}
		?>
	</div>
</div>
