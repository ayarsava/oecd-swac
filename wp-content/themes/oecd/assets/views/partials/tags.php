<?php
if ( empty( $pid ) ) {
	$pid = get_the_ID();
}

$sbx_taxonomies = array(
	'actions'             => array(
		'title' => 'Actions',
		'value' => wp_get_post_terms( $pid, 'actions' ),
	),
	'principles'          => array(
		'title' => 'Principles',
		'value' => wp_get_post_terms( $pid, 'principles' ),
	),
	'focus_areas'         => array(
		'title' => 'Focus areas',
		'value' => wp_get_post_terms( $pid, 'focus-areas' ),
	),
	'type_of_institution' => array(
		'title' => 'Type of institution',
		'value' => wp_get_post_terms( $pid, 'types-of-institution' ),
	),
);


$sbx_taxonomies = array_filter( $sbx_taxonomies );
$term_values    = array_column( $sbx_taxonomies, 'value' );

if ( ! empty( array_filter( $term_values ) ) ) {
	?>
	<div class="c-tags">
		<?php
		foreach ( $sbx_taxonomies as $sbx_taxonomy => $terms ) {

			if ( ! empty( $terms['value'] ) ) {
				echo '<div class="c-tags__item">';
				?>
				<span class="c-tags__title"><?php echo esc_html( $terms['title'] ); ?></span>

				<?php
				foreach ( $terms['value'] as $sbx_term ) {
					?>
					<a href="<?php echo esc_url( sbx_get_taxonomy_listing_page_link( $sbx_taxonomy, $sbx_term ) ); ?>" class="c-tags__button">
						<?php echo esc_html( $sbx_term->name ); ?>
					</a><br>
					<?php
				}
				echo '</div>';
			}

		}
		?>
	</div>
	<?php
}
