import $ from 'jquery';

/**
 * Content From Editor class.
 */

class ContentFromEditor {
    constructor() {
        this.$document = $(document);
        this.$contentFromEditor = $('.js-content-from-editor');
        this.$iframes = $('.js-content-from-editor p iframe');
        this.wrapIframe();
    }

    wrapIframe() {
        this.$iframes.wrap('<div class="iframe-container"></div>');
    }
}

export default ContentFromEditor;
