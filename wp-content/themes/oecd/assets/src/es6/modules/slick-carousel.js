import $ from 'jquery';

// npm install slick-carousel --save-dev
import 'slick-carousel';

class SlickCarousel {
    constructor () {
        this.$window = $(window);

        $('.js-carousel').slick({
            dots          : false,
            arrows        : true,
            infinite      : false,
            slidesToShow  : 6,
            slidesToScroll: 3,
            prevArrow     : '<button type="button" class="c-logo-carousel__button c-logo-carousel__button--prev slick-prev" aria-label="Previous"></button>',
            nextArrow     : '<button type="button" class="c-logo-carousel__button c-logo-carousel__button--next slick-next" aria-label="Next"></button>',
            responsive    : [
                {
                    breakpoint: 768,
                    settings  : {
                        infinite     : false,
                        speed        : 300,
                        centerMode   : false,
                        variableWidth: true,
                        arrows       : false,
                    },
                },
            ],
        });
    }
}

export default SlickCarousel;
