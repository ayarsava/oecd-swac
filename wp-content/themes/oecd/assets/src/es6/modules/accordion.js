/* global jQuery */
import $ from 'jquery';

class Accordion {
    constructor () {
        // Values from Drupal
        // this.settings = settings;
        // this.Drupal = Drupal;

        this.$window = $(window);

        // Elements
        this.$accordions       = $('.js-accordion');
        this.$accordionButtons = $('.js-accordion-button');
        this.$contents         = $('.js-accordion-content');

        // Events
        this.$window.on('load', () => {
            this.$contents.hide(

            );
            this.$accordionButtons.on('click', $.proxy(this.toggle, this));
        });
    }

    static booleanToggle (bool) {
        if (bool === true || bool === 'true') {
            return 'false';
        }

        return 'true';
    }

    toggle (e) {
        const $button     = $(e.currentTarget);
        const $buttonText = $button.find('.js-accordion-button-text');
        const $content    = $button.closest(this.$accordions).find(this.$contents);
        // Update heading ARIA
        $button.attr('aria-expanded', Accordion.booleanToggle($button.attr('aria-expanded'))).toggleClass('is-open');

        // Update content ARIA and slideToggle
        $content.attr('aria-hidden', Accordion.booleanToggle($content.attr('aria-hidden'))).slideToggle();

        // Blur button after closing
        if (!($button.hasClass('is-open'))) {
            $button.blur();
        }
    }
}

export default Accordion;
