import $ from 'jquery';

class Filters {
    constructor() {
        this.$document = $(document);
        this.$url = new URL(location);
        this.$params = new URLSearchParams(this.$url.search);
        this.$keyremover = $('.key-remover');
        this.$burgerButton = $('.js-burger-button');

        this.$keyremover.on('click', $.proxy(this.removeKey, this));
    }

    removeKey() {
        this.$url.searchParams.delete('key');
        history.replaceState(null, null, this.$url);
        location.reload();
    }
}

export default Filters;
