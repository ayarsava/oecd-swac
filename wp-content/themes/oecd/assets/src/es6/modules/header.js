import $ from 'jquery';
import Ps from 'perfect-scrollbar';
import debounce from './utils/debounce';

class Header {
    constructor() {
        this.$document = $(document);
        this.$window = $(window);
        this.$body = $('body');
        this.speed = 200;
        this.mobileBreakpoint = 1024;
        this.$main = $('main');
        this.$header = $('.js-header');
        this.headerHeight = this.$header.outerHeight(true);
        this.$content = $('.js-content');

        // Menu
        this.$mainMenu = $('.js-main-menu');

        // Burger Menu
        this.$burgerButton = $('.js-burger-button');
        this.$burgerContent = $('.js-burger-content');
        this.$subMenus = $('.js-sub-menu');

        // General sticky header
        this.$window.on('scroll', () => {
            this.stickyHeader();
        });

        // Burger Events
        this.$burgerButton.on('click', $.proxy(this.burgerToggle, this));


        this.$window.on('resize', debounce(() => {
            if (this.$window.width() > this.mobileBreakpoint) {
                this.resetEverything();
                this.destroyScrollbars();
            } else {
                this.addScrollbars();
            }
        }, 300)).on('load', () => {
            if (this.$window.width() <= this.mobileBreakpoint) {
                this.addScrollbars();
            }
        });

        // Scrollbars
        //----------------------------------------
        this.scrollbars = [];
    }

    addScrollbars() {
        this.scrollbars.push(new Ps(this.$burgerContent[0]));
    }

    destroyScrollbars() {
        this.scrollbars.forEach((ps) => ps.destroy());
    }

    updateScrollbars() {
        this.scrollbars.forEach((ps) => ps.update());
    }

    toggleMenu(e) {
        const $elem = $(e.currentTarget);

        $elem.toggleClass('is-open');
        $elem.parent().next().slideToggle(this.speed, () => {
            this.updateScrollbars();
        });
    }

    burgerToggle() {
        this.$burgerButton.toggleClass('is-open');
        this.$burgerContent.slideToggle(this.speed, () => {
            this.updateScrollbars();
        });
        this.$burgerButton.text(function (i, text) {
            return text === 'CLOSE' ? 'MENU' : 'CLOSE';
        });
        $('body').toggleClass('is-scroll-locked');
    }

    stickyHeader() {
        if (this.$content.length) {
            // Check if the page is using the WordPress admin bar. If it is, take the height of it away from the scrollTop to prevent jumpiness
            let windowScrollTop;
            if (this.$body.hasClass('admin-bar')) {
                windowScrollTop = this.$window.scrollTop() - 32;
            } else {
                windowScrollTop = this.$window.scrollTop();
            }

            if (windowScrollTop > (this.headerHeight)) {
                if (!this.$header.hasClass('is-sticky')) {
                    // Add class and negative top position to snap the sticky header out of view (64px is the height of the sticky header)
                    this.$header.addClass('is-sticky').css('top', '-88px');
                    // Reset the top position to 0 after the length of the sticky animation so it scrolls in and out of view properly
                    setTimeout(() => {
                        this.$header.css('top', 0);
                    }, 500);
                    this.$content.css('paddingTop', this.headerHeight);
                }
            } else {
                this.$header.removeClass('is-sticky');
                this.$content.css('paddingTop', 0);
            }

            if (windowScrollTop > (this.headerHeight + this.headerHeight)) {
                if (!this.$header.hasClass('sticky-is-visible')) {
                    this.$header.addClass('sticky-is-visible').css('top', 0);
                }
            } else {
                this.$header.removeClass('sticky-is-visible');
            }
        }
    }

    resetEverything() {
        // Reset Burger Menu
        this.$burgerButton.removeClass('is-open');
        this.$burgerContent.removeAttr('style');
        this.$subMenus.removeAttr('style');

        $('body').removeClass('is-scroll-locked');
    }
}

export default Header;
