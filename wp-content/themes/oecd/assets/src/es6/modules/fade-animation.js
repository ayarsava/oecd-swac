import $ from 'jquery';

// From inside `assets/src` run: `npm install intersection-observer --save-dev`
require('intersection-observer');

class FadeAnimation {
    constructor () {
        this.$elementsToFade = $('.js-fade');

        // Only fade in for the Story content type
        if ($('body').hasClass('single-story')) {
            if (this.$elementsToFade.length) {
                $(window).on('load', $.proxy(this.observeFadingElements, this));
            }
        }
    }

    observeFadingElements () {
        /**
         * # root
         * The element that is used as the viewport for checking visibility of
         * the target. Must be the ancestor of the target. Defaults to the
         * browser viewport if not specified or if null.
         *
         * # rootMargin
         * Margin around the root. Can have values similar to the CSS margin
         * property, e.g. "10px 20px 30px 40px" (top, right, bottom, left). The
         * values can be percentages. This set of values serves to grow or
         * shrink each side of the root element's bounding box before computing
         * intersections. Defaults to all zeros.
         *
         * # threshold
         * Either a single number or an array of numbers which indicate at what
         * percentage of the target's visibility the observer's callback should
         * be executed. If you only want to detect when visibility passes the
         * 50% mark, you can use a value of 0.5. If you want the callback to
         * run every time visibility passes another 25%, you would specify the
         * array [0, 0.25, 0.5, 0.75, 1]. The default is 0
         * (meaning as soon as even one pixel is visible, the callback will be
         * run). A value of 1.0 means that the threshold isn't considered
         * passed until every pixel is visible.
         */
        const options = {
            // root : null, // Default to viewport.
            rootMargin: '-100px 0px -100px 0px', // Elements must be 200px into
                                                 // the viewport top or bottom.
            // threshold : 0, // Any pixel of the element in view should
            // trigger the callback.
        };

        const currentPercentageObserver = new IntersectionObserver(
            (entries, observer) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        const elem = entry.target;

                        $(elem).addClass('is-animated');
                    }
                });
            }, options);

        // Observe each item
        for (let i = 0; i < this.$elementsToFade.length; i++) {
            const $elem = this.$elementsToFade[i];
            currentPercentageObserver.observe($elem);
        }
    }
}

export default FadeAnimation;
