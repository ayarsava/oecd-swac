/* global dataLayer */
import $ from 'jquery';

class HighlightToTweet {
    constructor (themeVariables) {
        this.$container     = $('.js-highlight-to-tweet');
        this.themeVariables = themeVariables;

        this.monitorMouseUp();
        this.monitorMouseDown();
        HighlightToTweet.monitorSelectionChange();
    }

    monitorMouseUp () {
        this.$container.on('mouseup', (e) => {
            let selection         = '';
            let originalSelection = '';
            let url               = '';
            let tweetHTML         = '';
            let tweetTextLength   = '';

            const mouseYpos = e.pageY;
            const mouseXpos = e.pageX;

            const targetTag     = e.target.tagName.toLowerCase();
            const tagsToExclude = [
                'input',
                'label',
                'textarea',
                'img',
                'svg',
            ];

            if (!$('.tweet-text').length && !tagsToExclude.includes(targetTag)) {
                selection         = HighlightToTweet.getSelection();
                originalSelection = selection;

                if (selection.length) {
                    url             = window.location.href;
                    tweetTextLength = 140 - url.length - 3;

                    // Push to the DataLayer so we can see it in GTM
                    HighlightToTweet.pushToDataLayer(originalSelection);

                    // If you can add more than five characters to the selection text (after adding the URL)
                    if (tweetTextLength > 5 && (selection.length > tweetTextLength)) {
                        selection = `${selection.substring(0, tweetTextLength)}...`;
                    }

                    selection         = `'${selection}'`;
                    originalSelection = `'${originalSelection}...'`;

                    const encodedSelection         = encodeURIComponent(selection);
                    const encodedUrl               = encodeURIComponent(url);
                    const encodedOriginalSelection = encodeURIComponent(`${originalSelection}`);
                    const siteName                 = encodeURIComponent('Coalition for Urban Transitions');

                    // The share links!
                    const twitterUrl  = `https://twitter.com/intent/tweet?text=${encodedSelection}&url=${encodedUrl}`;
                    const linkedInUrl = `https://www.linkedin.com/shareArticle?mini=true&text=${encodedSelection}&url=${encodedUrl}&source=${siteName}`;
                    const emailUrl    = `mailto:?subject=${encodedSelection}&body=${encodedOriginalSelection}${encodedUrl}`;

                    tweetHTML = `
                    <div class="c-highlight-to-tweet js-highlight-to-tweet-popup" style="top:${mouseYpos - 60}px; left:${mouseXpos - 20}px;">
                        <a class="c-highlight-to-tweet__twitter-btn js-social-share js-highlight-share" target="_blank" href="${twitterUrl}" title="Share the selected text on Twitter">                  
                            <svg class="o-svg o-svg--twitter">
                              <use class="o-svg__use " xlink:href="${this.themeVariables.templateUrl}/assets/img/symbol/svg/sprite.symbol.svg#twitter"/>
                            </svg>
                        </a>
                        
                        <a class="c-highlight-to-tweet__linkedin-btn js-social-share js-highlight-share" target="_blank" href="${linkedInUrl}" title="Share the selected text on LinkedIn">                  
                            <svg class="o-svg o-svg--linkedin">
                              <use class="o-svg__use " xlink:href="${this.themeVariables.templateUrl}/assets/img/symbol/svg/sprite.symbol.svg#linkedin"/>
                            </svg>
                        </a>
                        
                        <a class="c-highlight-to-tweet__email-btn js-social-share js-highlight-share" target="_blank" href="${emailUrl}" title="Share the selected text via email">
                            <svg class="o-svg o-svg--email">
                              <use class="o-svg__use " xlink:href="${this.themeVariables.templateUrl}/assets/img/symbol/svg/sprite.symbol.svg#email"/>
                            </svg>
                        </a>
                    </div>`;

                    $('body').append(tweetHTML);
                }
            }
        });
    }

    monitorMouseDown () {
        this.$container.on('mousedown', (e) => {
            // If the clicked element isn't part of the Tweet popup, remove the Tweet popup
            if (!$(e.target).parents('.js-highlight-to-tweet-popup').length) {
                $('.js-highlight-to-tweet-popup').remove();
            }
        });
    }

    static pushToDataLayer (selection) {
        if (typeof dataLayer !== 'undefined') {
            dataLayer.push({
                'event'         : 'textSelected',
                'selectedText'  : selection,
                'selectedLength': selection.length,
            });
        }
    }

    static monitorSelectionChange () {
        // Bugfix
        // If the selection changes and is empty, remove the popup
        // This bug occurs if you highlight a string, and then click on the highlighted string
        $(document).on('selectionchange', () => {
            const selection = HighlightToTweet.getSelection();

            if (!selection.length) {
                $('.js-highlight-to-tweet-popup').remove();
            }
        });
    }

    static getSelection () {
        let selection = '';

        if (window.getSelection) {
            selection = window.getSelection();
        } else if (document.selection) {
            selection = document.selection.createRange();
        }

        selection = selection.toString();

        return selection;
    }
}

export default HighlightToTweet;
