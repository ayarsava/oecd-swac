import $ from 'jquery';
import WindowPopup from './utils/window-popup';

class SocialSharing {
    constructor ($socialLinksSelector) {
        this.$socialLinksSelector = $socialLinksSelector;

        if (this.$socialLinksSelector.length > 0) {
            $('body').on('click', this.$socialLinksSelector, function (e) {
                e.preventDefault();

                WindowPopup($(this).attr('href'), 500, 300);
            });
        }
    }
}

export default SocialSharing;
