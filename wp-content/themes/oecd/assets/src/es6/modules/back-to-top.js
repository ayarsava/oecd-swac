import $ from 'jquery';

class BackToTop {
    constructor () {
        this.$bttbutton = $('.js-top');

        // Events
        this.$bttbutton.on('click', () => {
            window.scrollTo({
                top     : -100,
                left    : 0,
                behavior: 'smooth',
            });
        });
    }
}

export default BackToTop;
