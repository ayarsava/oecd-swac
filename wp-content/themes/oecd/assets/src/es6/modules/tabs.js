// Based on: https://inclusive-components.design/tabbed-interfaces/

import $ from 'jquery';

class Tabs {
    constructor () {
        // Elements
        this.$wrapper       = $('.js-tabs'); // tabbed
        this.$tabButtons    = this.$wrapper.find('.js-tabs-button'); // tabs
        this.$mobileButtons = this.$wrapper.find('.js-tabs-mobile-button'); // tabs
        this.$panels        = this.$wrapper.find('.js-tabs-panel'); // panels

        // Events
        this.$tabButtons.on('click', $.proxy(this.tabClick, this));
        this.$tabButtons.on('keydown', $.proxy(this.keydown, this));
        this.$mobileButtons.on('click', $.proxy(Tabs.mobileClick, this));

        // Set the current tab
        this.$currentTab   = $(this.$tabButtons[0]);
        this.$currentPanel = $(this.$panels[0]);
    }

    tabClick (e) {
        e.preventDefault();

        const $elem = $(e.currentTarget);

        // If we're clicking the non active tab, switch!
        if ($elem[0] !== this.$currentTab[0]) {
            this.switchTab($elem);
        }
    }

    static mobileClick (e) {
        e.preventDefault();

        const $elem = $(e.currentTarget);

        $elem.toggleClass('is-open');
        $elem.attr('aria-expanded', ($elem.hasClass('is-open')));
    }

    keydown (e) {
        // Get the index of the current tab in the tabs node list
        const index = Array.prototype.indexOf.call(this.$tabButtons, e.currentTarget);

        let direction = null;

        // Work out which key the user is pressing and
        // Calculate the new tab's index where appropriate

        // Left arrow
        if (e.which === 37) {
            direction = index - 1;
        }

        // Right arrow
        if (e.which === 39) {
            direction = index + 1;
        }

        // Down arrow
        if (e.which === 40) {
            direction = 'down';
        }

        if (direction !== null) {
            e.preventDefault();

            // If the `down` key is pressed, move focus to the open panel,
            // otherwise switch to the adjacent tab
            if (direction === 'down') {
                // Focus on the active panel!
                this.$panels.find('is-active').focus();
            }

            if (direction !== 'down' && this.$tabButtons[direction]) {
                // Move side to side!
                this.switchTab($(this.$tabButtons[direction]));
            }
        }
    }

    // The tab switching function
    switchTab ($newTab) {
        // Deselect the old tab
        this.$currentTab.removeAttr('aria-selected');
        this.$currentTab.attr('tabindex', '-1');
        this.$currentTab.removeClass('is-active');
        this.$currentTab.parent().removeClass('is-active');

        // Select the new tab
        this.$currentTab = $newTab;
        this.$currentTab.attr('aria-selected', 'true');
        this.$currentTab.removeAttr('tabindex');
        this.$currentTab.addClass('is-active');
        this.$currentTab.parent().addClass('is-active');
        this.$currentTab.focus();

        // Switch panels
        this.switchPanel(this.$currentTab.attr('href'));
    }

    switchPanel (targetPanel) {
        // Deselect the old panel
        this.$currentPanel.attr('aria-hidden', 'true');
        this.$currentPanel.attr('tabindex', '-1');
        this.$currentPanel.removeClass('is-active');

        // Select the new panel
        this.$currentPanel = this.$wrapper.find(targetPanel);
        this.$currentPanel.attr('aria-hidden', 'false');
        this.$currentPanel.removeAttr('tabindex');
        this.$currentPanel.addClass('is-active');
    }
}

export default Tabs;
