/* global dataLayer */
import $ from 'jquery';

// npm install lory.js --save-dev
import {lory} from 'lory.js';

class Slider {
    constructor () {
        this.$sliders  = $('.js-slider');
        this.$slides   = $('.js-slider__slide');
        this.$counters = $('.js-slider__counter');

        // Init each slider
        if (this.$sliders.length) {
            for (let i = 0, l = this.$sliders.length; i < l; i++) {
                const slider = this.$sliders[i];

                slider.addEventListener('before.lory.init', $.proxy(this.initCounter, this));
                slider.addEventListener('after.lory.slide', $.proxy(this.updateCounter, this));

                lory(slider, {
                    classNameFrame         : 'js-slider__frame',
                    classNameSlideContainer: 'js-slider__slides',
                    classNamePrevCtrl      : 'js-slider__prev',
                    classNameNextCtrl      : 'js-slider__next',
                    infinite               : 1, // False or a number
                });
            }
        }
    }

    initCounter (e) {
        const $elem   = $(e.currentTarget);
        const $slides = $elem.find(this.$slides);

        $elem.find(this.$counters).text(`1/${$slides.length}`);

        this.pushToDataLayer($elem);
    }

    updateCounter (e) {
        const $elem   = $(e.currentTarget);
        const $slides = $elem.find(this.$slides);
        const index   = $slides.index($slides.filter('.active'));

        $elem.find(this.$counters).text(`${index + 1}/${$slides.length}`);

        this.pushToDataLayer($elem);
    }

    pushToDataLayer ($elem) {
        const $slider     = $elem.closest(this.$sliders);
        const sliderIndex = this.$sliders.index($slider);

        if (typeof dataLayer !== 'undefined') {
            dataLayer.push({
                'event'         : 'sliderChange',
                'sliderPosition': `Slider ${sliderIndex + 1}: ${$elem.find(this.$counters).text()}`,
            });
        }
    }
}

export default Slider;
