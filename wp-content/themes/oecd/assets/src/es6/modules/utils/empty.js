export default function empty (checkVal) {
    let undef;
    let key;
    const emptyValues = [
        undef,
        undefined,
        'undefined',
        null,
        false,
        0,
        '',
        '0',
    ];

    const len = emptyValues.length;

    for (let i = 0; i < len; i += 1) {
        if (checkVal === emptyValues[i]) {
            return true;
        }
    }

    if (typeof checkVal === 'object') {
        for (key in checkVal) {
            if (checkVal.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    return false;
}
