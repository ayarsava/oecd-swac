/* global jQuery, themeVariables */
import jQuery from 'jquery';
import Svg4Everybody from 'svg4everybody';
//import Ps from 'perfect-scrollbar';
import objectFitImages from 'object-fit-images';
import AOS from 'aos';

// Relative Imports
import Filters from './modules/filters';
import Header from './modules/header';
import ContentFromEditor from "./modules/media";
import Pym from "./modules/pym";
import $ from "jquery";

(function ($) {
    const debug = true;
    const mobileBreakpoint = 747;
    /**
     * Header
     */
    new Header();

    /**
     * Filters
     */
    new Filters('.js-filters');

    /**
     * SVG4Everybody
     */
    new Svg4Everybody();

    /**
     * Object fit polyfill
     */
    objectFitImages();

    /**
     * ContentFromEditor
     */
    new ContentFromEditor();

    /**
     * Pym
     */
    new Pym();


}(jQuery));

window.onload = function () {
    var jsTabs = document.getElementById("js-get-url");
    if (jsTabs) {
        var all_links = document.getElementById("js-get-url").getElementsByTagName("a"),
            i = 0, len = all_links.length,
            full_path = location.href.split('#')[0]; //Ignore hashes?

        // Loop through each link.
        for (; i < len; i++) {
            if (all_links[i].href.split("#")[0] == full_path) {
                all_links[i].className += " active";
            }
        }
    }
    AOS.init({
        duration: 1200,
    })
}
