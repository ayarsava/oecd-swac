<?php
/**
 * General CMS admin/backend configuration.
 */

/**
 * Rename WordPress default menu items.
 */
function edit_admin_menus()
{

    global $menu;
    global $submenu;

    // phpcs:disable
    if (!empty($menu[5])) {
        $menu[5][0] = 'Stories';
    }

    if (!empty($submenu['edit.php'])) {

        $submenu['edit.php'][5][0] = 'All items';
        $submenu['edit.php'][10][0] = 'Add New';
        //$submenu['edit.php'][16][0] = 'Tags';
        echo '';
    }

    if (!empty($menu[10])) {
        $menu[10][0] = 'Uploads';
        //$submenu['upload.php'][5][0]  = 'Library';
        //$submenu['upload.php'][10][0] = 'Add New';
        echo '';
    }

    //remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
    //remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
    // phpcs:enable
}

add_action('admin_menu', 'edit_admin_menus');

// Disable Categories and Tags.
// phpcs:disable
function unregister_categories_and_tags()
{

    register_taxonomy('post_tag', []);
    unregister_widget('WP_Widget_Tags');

    register_taxonomy('category', []);
    unregister_widget('WP_Widget_Categories');
}

add_action('init', 'unregister_categories_and_tags');
// phpcs:enable


/**
 * Customise the menu order
 */
function custom_menu_order($menu_ord)
{
    if (!$menu_ord) {
        return true;
    }

    return array(
        // phpcs:ignore
        /*
            'index.php', // Dashboard
            'separator1', // First separator
            'edit.php', // Posts
            'upload.php', // Media
            'link-manager.php', // Links
            'edit.php?post_type=page', // Pages
            'edit-comments.php', // Comments
            'separator2', // Second separator
            'themes.php', // Appearance
            'plugins.php', // Plugins
            'users.php', // Users
            'tools.php', // Tools
            'options-general.php', // Settings
            'separator-last', // Last separator
        */

        // Dashboard.
        'index.php',

        // First separator.
        'separator1',

        // Pages.
        'edit.php?post_type=page',

        // Custom post types.
        'edit.php?post_type=report',
        'edit.php', // Posts.

        // Second separator.
        'separator2',

        // Media.
        'upload.php',

        // Comments.
        'edit-comments.php',

        // Appearance.
        'themes.php',

        // Plugins.
        'plugins.php',

        // Users.
        'users.php',

        // Tools.
        'tools.php',

        // Settings.
        'options-general.php',
    );
}

add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');

/**
 * Allow all files types to be uploaded to posts
 */
function update_edit_form()
{

    echo ' enctype="multipart/form-data"';
}

add_action('post_edit_form_tag', 'update_edit_form');

/**
 * Enable Excerpts on Page post type
 */
function add_page_excerpts()
{

    add_post_type_support('page', 'excerpt');
}

add_action('init', 'add_page_excerpts');

/**
 * Function to change Posts to News
 */
function sb_change_post_object()
{

    global $wp_post_types;

    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Stories';
    $labels->singular_name = 'Story';
    $labels->add_new = 'Add Stories';
    $labels->add_new_item = 'Add Stories Item';
    $labels->edit_item = 'Edit Stories Item';
    $labels->new_item = 'Stories Item';
    $labels->view_item = 'View Stories Item';
    $labels->search_items = 'Search Stories Item';
    $labels->not_found = 'No Stories Items found';
    $labels->not_found_in_trash = 'No Stories Items found in Trash';
    $labels->all_items = 'All Stories Items';
    $labels->menu_name = 'Stories Items';
    $labels->name_admin_bar = 'Stories Item';
}

add_action('init', 'sb_change_post_object');

/**
 * Register and enqueue a custom Javascript in the WordPress admin.
 *
 * function sbx_enqueue_custom_admin_scripts() {
 * wp_register_script( 'custom_wp_admin_js', get_template_directory_uri() . '/assets/js/admin.js', false, '1.0', true );
 * wp_enqueue_script( 'custom_wp_admin_js' );
 * }
 *
 * add_action( 'admin_enqueue_scripts', 'sbx_enqueue_custom_admin_scripts' );
 */

/**
 * Register and enqueue a custom CSS in the WordPress admin.
 */
function sbx_enqueue_custom_admin_style()
{
    wp_register_style('custom_wp_admin_css', get_template_directory_uri() . '/assets/css/admin-style.css', false, '1.0');
    wp_enqueue_style('custom_wp_admin_css');
}

add_action('admin_enqueue_scripts', 'sbx_enqueue_custom_admin_style');

/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 */
function add_editor_styles()
{
    add_editor_style('/assets/css/tinymce-style.css');
}

add_action('init', 'add_editor_styles');

/**
 * Remove WP Logo from Admin bar
 */
function wp_admin_bar_remove()
{
    global $wp_admin_bar;

    /* Remove wp_logo */
    $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'wp_admin_bar_remove', 0);


/**
 * Remove WordPress News from dashboard.
 */
function remove_dashboard_meta()
{

    remove_meta_box('dashboard_primary', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');

    // remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    // remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    // remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
}

add_action('admin_init', 'remove_dashboard_meta');
