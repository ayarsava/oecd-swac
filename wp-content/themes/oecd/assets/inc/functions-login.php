<?php
/**
 * Customisations to the WordPress login page.
 */

/**
 * Custom Login logo and link
 */
function sbx_customise_login_logo() {
	?>
	<style>
        body.login {
            background-color: #fdfcf3;
        }

        body.login form {
            border-color: #fdfcf3;
        }

        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.svg);
            -webkit-background-size: 176px;
            background-size: 266px;
            height: 68px;
            padding-bottom: 10px;
            width: 266px;
            height: 68px;
        }

        body.login #wp-submit {
            background: #263c4e;
            border-color: #263c4e;
            border-radius: 0;
            width: 100%;
            min-height: 50px;
            margin-top: 10px;
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 1px;
            text-transform: uppercase;
        }

        body.login #wp-submit:hover {
            background: #e39659;
            border-color: #e39659;
        }

        body.login form input {
            border-radius: 0;
        }

        input[type=checkbox]:focus,
        input[type=color]:focus,
        input[type=date]:focus,
        input[type=datetime-local]:focus,
        input[type=datetime]:focus,
        input[type=email]:focus,
        input[type=month]:focus,
        input[type=number]:focus,
        input[type=password]:focus,
        input[type=radio]:focus,
        input[type=search]:focus,
        input[type=submit]:focus,
        input[type=tel]:focus,
        input[type=text]:focus,
        input[type=time]:focus,
        input[type=url]:focus,
        input[type=week]:focus,
        select:focus,
        textarea:focus, .wp-core-ui .button-primary:focus {
            border-color: #F0B333;
            -webkit-box-shadow: 0 0 2px rgba(240, 179, 51, .8);
            box-shadow: 0 0 2px rgba(240, 179, 51, .8);
        }

        input[type=checkbox]:checked:before {
            content: '\f147';
            margin: -3px 0 0 -4px;
            color: #692480;
        }

        .login a:hover {
            color: #692480 !important;
        }
	</style>
	<?php
}

add_action( 'login_enqueue_scripts', 'sbx_customise_login_logo' );

/**
 * Change the logo URL
 *
 * @return string|void
 */
function sbx_custom_login_logo_url() {

	return home_url();
}

add_filter( 'login_headerurl', 'sbx_custom_login_logo_url' );

/**
 * Change the <title> tag.
 *
 * @return string|void
 */
function sbx_custom_login_logo_url_title() {

	return get_bloginfo( 'name' );
}

add_filter( 'login_headertext', 'sbx_custom_login_logo_url_title' );

