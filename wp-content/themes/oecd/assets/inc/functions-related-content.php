<?php
/**
 * Functions to help with Related Content selection.
 *
 * SBTODO: To use this file fill in the $allowed_taxonomies array below.
 */

// ACF Setup:
// Field: automatic_related_content
// enabled : Enabled
// disabled : Disabled

// Field: manual_related_content
// repeater
// - related item
// - - Post Object, return post ID

/**
 * Function to handle the retrieval of an array of related content based on:
 * - Manual related content
 * - Posts where this post is related content
 * - Posts with a similar taxonomy selection
 *
 * @param $post_id
 *
 * @return array
 */
function get_related_content( $post_id = null, $limit = 3, $excluded_items = [] ) {

	// Debugging Switch.
	$debug = false;

	if ( empty( $post_id ) ) {
		$post_id = get_the_ID();
	}

	// Is related content enabled for this post?
	$disable_auto_related_content = get_field( 'automatic_related_content', $post_id );

	// Get manually selected items.
	$related_content = get_manual_related_content( $post_id );

	if ( $disable_auto_related_content !== 'disabled' ) {
		if ( count( $related_content ) < $limit ) {
			// Get posts where this post is related.
			$related_content = array_merge( $related_content, get_related_posts_by_related( $post_id, $limit, $excluded_items ) );
		}

		$excluded_items = array_merge( $related_content, $excluded_items );

		if ( count( $related_content ) < $limit ) {
			// Get posts with exactly the same taxonomy selections.
			$related_content = array_merge( $related_content, get_related_posts_by_taxonomy( $post_id, $limit, $excluded_items, 'AND', 'AND' ) );
		}

		$excluded_items = array_merge( $related_content, $excluded_items );

		if ( count( $related_content ) < $limit ) {
			// Get posts with the same taxonomy selections.
			$related_content = array_merge( $related_content, get_related_posts_by_taxonomy( $post_id, $limit, $excluded_items, 'AND', 'IN' ) );
		}

		$excluded_items = array_merge( $related_content, $excluded_items );

		if ( count( $related_content ) < $limit ) {
			// Get posts with similar taxonomy selections.
			$related_content = array_merge( $related_content, get_related_posts_by_taxonomy( $post_id, $limit, $excluded_items ) );
		}

		$excluded_items = array_merge( $related_content, $excluded_items );

		if ( count( $related_content ) < $limit ) {
			// Get related content with similar post type.
			$related_content = array_merge( $related_content, get_related_posts_by_post_type( $post_id, $limit, $excluded_items ) );
		}

		// Check related content doesn't exceed the expected limit.
		if ( count( $related_content ) > $limit ) {
			$related_content = array_slice( $related_content, 0, $limit );
		}
	}

	// Need to debug? Let's see what's actually happening.
	//phpcs:disable
	if ( $debug ) {
		echo '<h2>Show auto related content?</h2>';
		echo '<pre>';
		var_dump( $disable_auto_related_content );
		echo '</pre>';

		echo '<h2>Manual Related content:</h2>';
		echo '<pre>';
		var_dump( get_manual_related_content( $post_id ) );
		echo '</pre>';

		echo '<hr/><h2>Posts where this post is set as a related item:</h2>';
		echo '<pre>';
		var_dump( get_related_posts_by_related( $post_id ) );
		echo '</pre>';

		echo '<hr/><h2>Posts with exactly the same taxonomies:</h2>';
		echo '<pre>';
		var_dump( get_related_posts_by_taxonomy( $post_id, null, null, 'AND', 'AND' ) );
		echo '</pre>';

		echo '<hr/><h2>Posts with the same taxonomies:</h2>';
		echo '<pre>';
		var_dump( get_related_posts_by_taxonomy( $post_id, null, null, 'AND', 'IN' ) );
		echo '</pre>';

		echo '<hr/><h2>Posts with similar taxonomies:</h2>';
		echo '<pre>';
		var_dump( get_related_posts_by_taxonomy( $post_id ) );
		echo '</pre>';

		echo '<hr/><h2>Posts with same post type:</h2>';
		echo '<pre>';
		var_dump( get_related_posts_by_post_type( $post_id ) );
		echo '</pre>';

		echo '<hr/><h2>Related Content:</h2>';
		echo '<pre>';
		var_dump( $related_content );
		echo '</pre>';
	}

	//phpcs:enable

	return $related_content;
}

/**
 * Get an array of post_id's selected in ACF's related content field
 *
 * @param $post_id
 *
 * @return array
 */
function get_manual_related_content( $post_id ) {

	// ACF related content selections.
	$manual_related_content = get_field( 'manual_related_content', $post_id );

	// Return an empty array if none set.
	if ( empty( $manual_related_content ) ) {
		return array();
	}

	return array_filter( array_column( $manual_related_content, 'related_item' ) );
}

/**
 * Get an array of post_id's where this post has been selected as an item of related content
 *
 * @param null  $post_id
 * @param array $excluded_posts
 *
 * @return array
 */
function get_related_posts_by_related( $post_id = null, $related_count = 3, $excluded_posts = [] ) {

	global $wpdb;

	if ( empty( $post_id ) ) {
		$post_id = get_the_ID();
	}

	// Args
	// These args will be updated by the related_like_where filter.
	$args = array(
		'fields'         => 'ids',
		'posts_per_page' => $related_count,
		'post_type'      => 'any',
		// phpcs:ignore
		'meta_query'     => array(
			array(
				'key'     => 'manual_related_content_$_related_item',
				'compare' => '=',
				// This is converted to a WHERE by the related_like_where filter.
				'value'   => $post_id,
			),
		),
		// phpcs:disable
		// Custom Exclude:
		// 'tax_query'   => [
		//     [
		//         'taxonomy' => 'conference-page-type',
		//         'field'    => 'slug',
		//         'terms'    => [
		//             'default',
		//             'live-blog',
		//             'programme',
		//             'speakers',
		//             'sponsors',
		//             'venue'
		//         ],
		//         'operator' => 'NOT IN',
		//     ]
		// ]
		// phpcs:enable
	);

	// Query.
	$query_results = new WP_Query( $args );

	$rows = $query_results->posts;

	// Return an empty array if none found.
	if ( empty( $rows ) ) {
		return array();
	}

	// Store only the IDs.
	// $related_ids = array_column($rows, 'post_id');
	$related_ids = $rows;

	// Add current post to excluded array.
	$excluded_posts[] = $post_id;

	// Return non-excluded ids.
	return array_map( 'intval', array_diff( $related_ids, $excluded_posts ) );
}

/**
 * Get an array of post_ids of posts with a similar taxonomy selection
 *
 * @param        $post_id
 * @param int    $related_count
 * @param array  $excluded_posts
 * @param string $operator
 * @param string $term_operator
 *
 * @return array
 */
function get_related_posts_by_taxonomy( $post_id, $related_count = 3, $excluded_posts = [], $operator = 'OR', $term_operator = 'IN' ) {

	// Add current post to excluded array.
	$excluded_posts[] = $post_id;

	// Base related args.
	$related_args = array(
		'fields'         => 'ids',
		'orderby'        => 'rand',
		'post_type'      => 'any',
		'posts_per_page' => $related_count,
		'post_status'    => 'publish',
		'post__not_in'   => $excluded_posts,
		// phpcs:disable
		// Custom Exclude
		// 'tax_query'      => [
		//     [
		//         'taxonomy' => 'conference-page-type',
		//         'field'    => 'slug',
		//         'terms'    => [
		//             'default',
		//             'live-blog',
		//             'programme',
		//             'speakers',
		//             'sponsors',
		//             'venue'
		//         ],
		//         'operator' => 'NOT IN',
		//     ]
		// ]
		// phpcs:enable
	);

	// Get this post's taxonomy information.
	$has_terms  = false;
	$post       = get_post( $post_id );
	$taxonomies = get_object_taxonomies( $post, 'names' );

	$allowed_taxonomies = array(
		'topics',
	);
	$taxonomies         = array_intersect( $taxonomies, $allowed_taxonomies );

	if ( ! empty( $taxonomies ) ) {
		foreach ( $taxonomies as $taxonomy ) {
			$terms = get_the_terms( $post_id, $taxonomy );

			// Skip if no terms are found.
			if ( empty( $terms ) ) {
				continue;
			}

			$has_terms = true;
			$term_list = wp_list_pluck( $terms, 'slug' );

			// Add a tax query to the args for each taxonomy this post has.
			if ( $operator === 'AND' ) {
				$term_operator = 'AND';
			}

			$related_args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $term_list,
				'operator' => $term_operator,
			);
		}

		// Use an AND or OR operator?.
		if ( ! empty( $related_args['tax_query'] ) && count( $related_args['tax_query'] ) > 1 ) {
			$related_args['tax_query']['relation'] = $operator;
		}

		if ( $has_terms ) {
			$related_content = get_posts( $related_args );
		}
	}

	// Return an empty array if none found.
	if ( empty( $related_content ) ) {
		$related_content = array();
	}

	return $related_content;
}

function get_related_posts_by_post_type( $post_id, $limit = 3, $excluded_posts = array() ) {

	if ( empty( $post_id ) ) {
		$post_id = get_the_ID();
	}

	$post_type = get_post_type( $post_id );

	// SBTODO: Deal with hierarchical CPTs
	// if ($post_type === 'conference') {
	//  $post_id = get_top_parent_id($post_id);
	// }

	$excluded_posts[] = $post_id;

	$args = array(
		'fields'         => 'ids',
		'posts_per_page' => $limit,
		'post_type'      => $post_type,
		'post_parent'    => 0,
		'post__not_in'   => $excluded_posts,
		'order'          => 'ASC',
		'orderby'        => 'sb_post_date',
	);

	// Query.
	$query_results   = new WP_Query( $args );
	$related_content = $query_results->posts;

	return $related_content;
}

/**
 * Modify the SQL before it is executed to convert related content meta_queries into wheres
 *
 * @param $where
 *
 * @return mixed
 */
function my_posts_where( $where ) {

	$where = str_replace( "meta_key = 'manual_related_content_$", "meta_key LIKE 'manual_related_content_%", $where );

	return $where;
}

add_filter( 'posts_where', 'my_posts_where' );
