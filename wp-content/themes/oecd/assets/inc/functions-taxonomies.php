<?php
/**
 * Declare custom taxonomies in this file!
 */

/**
 * Register taxonomies and create default terms.
 */
function sbx_register_tax()
{
    create_topics_taxonomy();
    create_types_taxonomy();

    // Create Default Taxonomy Terms.
    sbx_add_default_terms();

    // Reset.
    update_option('initial_taxes_created', false);
}

add_action('init', 'sbx_register_tax', 0);

/**
 * Register principles taxonomy (hierarchical like categories)
 */
function create_topics_taxonomy()
{
    $plural = __('Topics', 'text_domain');
    $singular = __('Topic', 'text_domain');

    // Set label names.
    // phpcs:disable
    $labels = array(
        'name' => $plural,
        'singular_name' => $singular,
        'search_items' => sprintf(__('Search %s', 'text_domain'), $plural),
        'all_items' => sprintf(__('All %s', 'text_domain'), $plural),
        'parent_item' => sprintf(__('Parent %s', 'text_domain'), $singular),
        'parent_item_colon' => sprintf(__('Parent %s:', 'text_domain'), $plural),
        'edit_item' => sprintf(__('Edit %s', 'text_domain'), $singular),
        'update_item' => sprintf(__('Update %s', 'text_domain'), $singular),
        'add_new_item' => sprintf(__('Add New %s', 'text_domain'), $singular),
        'new_item_name' => sprintf(__('New %s Name', 'text_domain'), $singular),
        'menu_name' => $plural,
    );
    // phpcs:enable

    // Register the taxonomy.
    register_taxonomy(
        sanitize_title(strtolower($plural)),
        array(
            'post',
            'report',
            'page',
        ),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            // 'show_in_rest' =>true, // Uncomment for Gutenberg.
            'query_var' => true,
            'rewrite' => array('slug' => sanitize_title(strtolower($singular))),
        )
    );
}


/**
 * Register format taxonomy (hierarchical like categories)
 */
function create_types_taxonomy()
{
    $plural = __('Types', 'text_domain');
    $singular = __('Type', 'text_domain');

    // Set label names.
    // phpcs:disable
    $labels = array(
        'name' => $plural,
        'singular_name' => $singular,
        'search_items' => sprintf(__('Search %s', 'text_domain'), $plural),
        'all_items' => sprintf(__('All %s', 'text_domain'), $plural),
        'parent_item' => sprintf(__('Parent %s', 'text_domain'), $singular),
        'parent_item_colon' => sprintf(__('Parent %s:', 'text_domain'), $plural),
        'edit_item' => sprintf(__('Edit %s', 'text_domain'), $singular),
        'update_item' => sprintf(__('Update %s', 'text_domain'), $singular),
        'add_new_item' => sprintf(__('Add New %s', 'text_domain'), $singular),
        'new_item_name' => sprintf(__('New %s Name', 'text_domain'), $singular),
        'menu_name' => $plural,
    );
    // phpcs:enable

    // Register the taxonomy.
    register_taxonomy(
        sanitize_title(strtolower($plural)),
        array(
            'post',
        ),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            // 'show_in_rest' =>true, // Uncomment for Gutenberg.
            'query_var' => true,
            'rewrite' => array('slug' => sanitize_title(strtolower($singular))),
        )
    );
}


/**
 * Add default taxonomy terms to the CMS.
 */
function sbx_add_default_terms()
{
    if (get_option('initial_taxes_created') !== '1') {
        $taxonomies = array(
            'topics' => array(
                'Climate',
                'Gender',
                'Security',
            ),
            'types' => array(
                'Audio',
                'Blog post',
                'Video',
            ),
        );

        foreach ($taxonomies as $taxonomy => $terms) {
            foreach ($terms as $term_label) {
                $term = term_exists($term_label, $taxonomy);

                // If the term doesn't exist, create it.
                if (empty($term)) {
                    $term = wp_insert_term(
                        $term_label,
                        $taxonomy,
                        array(
                            'description' => $term_label,
                            'slug' => sanitize_title($term_label),
                        )
                    );
                }
            }
        }

        update_option('initial_taxes_created', true);
    }
}

// Removing global taxonomies boxes
function remove_taxonomy_boxes()
{

    remove_meta_box('typesdiv', 'report', 'side');
    remove_meta_box('postexcerpt', 'page', 'normal');

}

add_action('admin_menu', 'remove_taxonomy_boxes');
