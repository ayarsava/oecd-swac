<?php
/**
 * General CMS and Theme configuration.
 */

/**
 * Disable XML-RPC.php
 */
add_filter('xmlrpc_enabled', '__return_false');

/**
 * Disable WP REST API / wp-json
 */
add_filter('json_enabled', '__return_false');
add_filter('json_jsonp_enabled', '__return_false');

/**
 * Move scripts to the footer
 */
function move_scripts_to_footer()
{
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}

add_action('wp_enqueue_scripts', 'move_scripts_to_footer');

/**
 * Theme CSS
 */
function load_theme_styles()
{
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Libre+Franklin:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700&display=swap', false);
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Source+Serif+Pro:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap', false);

    if (is_page_template('page.php')) {
        if (function_exists('wpcf7_enqueue_styles')) {
            wpcf7_enqueue_styles();
        }
    }

    wp_enqueue_style('theme-styles', get_template_directory_uri() . '/assets/css/style.min.css', array(), 1.1, 'all');
}

add_action('wp_enqueue_scripts', 'load_theme_styles');

/**
 * Theme Javascript
 */
function load_theme_scripts()
{
    wp_register_script('theme-script', get_template_directory_uri() . '/assets/js/script.min.js', array('jquery'), 1.2, true);

    wp_enqueue_script('theme-script');

    // Prepare variables to be passed from PHP to the JS file.
    $script_variables = array(
        'templateUrl' => get_stylesheet_directory_uri(),
    );

    wp_localize_script('theme-script', 'themeVariables', $script_variables);

    if (is_page_template('page.php')) {
        if (function_exists('wpcf7_enqueue_scripts')) {
            wpcf7_enqueue_scripts();
        }
    }

}

add_action('wp_enqueue_scripts', 'load_theme_scripts');

/**
 * Register image sizes
 */
add_image_size('logo-image', 820, 820, false);
add_image_size('profile-image', 328, 328, true);
add_image_size('full-width', 820, 420, true);
add_image_size('full-width-retina', 1640, 840, false);

/**
 * Convert JPEG and PNG to WebP on upload.
 *
 * add_filter(
 * 'image_editor_output_format',
 * function ( $formats ) {
 * $formats['image/jpeg'] = 'image/webp';
 * $formats['image/png']  = 'image/webp';
 *
 * return $formats;
 * }
 * ); */

/**
 * Add Custom image Sizes to Add Media dropdown
 *
 * @param array $sizes Array of sizes the WYSIWYG media modal window is aware of.
 *
 * @return array
 */
function custom_media_sizes(array $sizes): array
{

    return array_merge(
        $sizes,
        array(
            'full-width' => __('Full Width'),
            'full-width-retina' => __('Full Width (Retina)'),
        )
    );
}

add_filter('image_size_names_choose', 'custom_media_sizes');

/**
 * Register menu areas
 */
function register_menus()
{

    register_nav_menus(
        array(
            'main-menu' => __('Main Menu'),
            'footer-menu' => __('Footer Menu'),
        )
    );
}

add_action('init', 'register_menus');

/**
 * Add Thumbnail support to the theme
 */
add_theme_support('post-thumbnails');

/**
 * Enable support for HTML5 markup.
 */
add_theme_support(
    'html5',
    array(
        'comment-list',
        'comment-form',
        'search-form',
        'gallery',
        'caption',
        'style',
        'script',
    )
);

/**
 * Enables plugins and themes to manage the document title tag.
 */
add_theme_support('title-tag');

/**
 * Modify the output of wp_title.
 *
 * @param $a
 * @param $b
 *
 * @return string
 */
function sbx_customise_title($a, $b): string
{
    // Title Variables.
    $blog_name = get_bloginfo('name');

    if (is_front_page()) {
        $blog_description = get_bloginfo('description');
    } elseif (is_search()) {
        // Canonical for search pages should be without GET params.
        $search_query = get_search_query();

        if (!empty($search_query)) {
            $blog_description = 'Search results for \'' . $search_query . '\'';
        } else {
            $blog_description = 'Search';
        }
    } elseif (is_404()) {
        $blog_description = '404 Page not found';
    } else {
        $blog_description = get_the_title();
    }

    $title = '';

    if (!empty($blog_description)) {
        $title .= esc_textarea($blog_description) . ' | ';
    }

    $title .= esc_attr($blog_name);

    return $title;
}

add_filter('wp_title', 'sbx_customise_title', 10, 2);

/**
 * Force search even if 's' is empty
 *
 * @param WP_Query $query The global WP_Query instance.
 */
function search_even_empty(WP_Query $query)
{

    // phpcs:ignore
    if (isset($_GET['s'])) {
        $query->is_search = true;
    }
}

add_action('parse_query', 'search_even_empty');

/**
 * Rewrite all search URL's to /search/[SEARCH_TERM]?[REST_OF_PARAMS]
 */
function change_search_url_rewrite()
{
    // phpcs:ignore
    if (is_search() && isset($_GET['s'])) {
        $query = filter_input_array(INPUT_GET);

        if (isset($query['s'])) {
            $s = rawurlencode($query['s']);
            unset($query['s']);
        } else {
            $s = '';
        }

        $query = http_build_query($query, '', '&', PHP_QUERY_RFC3986);

        if (!empty($query) && mb_strpos($query, '?') !== 0) {
            $query = '?' . $query;
        }

        wp_safe_redirect(home_url('/search/') . $s . $query);
        exit();
    }
}

add_action('template_redirect', 'change_search_url_rewrite');

/**
 * This function fixes the class for the navigation, when custom post types are involved
 * Get the current pages parent by checking against the url
 *
 * @param array $classes Array of classes.
 * @param mixed $item Usually a WP_Post item, or an array for custom links.
 *
 * @return array Updated array of classes.
 */
function apply_parent_nav_class(array $classes, $item): array
{

    // Get the page url.
    // phpcs:ignore
    //$page_url = get_permalink();

    // Get the menu items title and make it lowercase
    // Then replace all space type characters with dashes
    // And replace any instance of '&' with 'and'.
    $item_title = str_replace(site_url('/'), '/', rtrim($item->url, '/'));

    $post_type = get_post_type();
    $page_url = get_site_url();

    // Force highlight on these pages.
    // phpcs:ignore
    if (get_the_ID() === 188) {
        // phpcs:ignore
        // $post_type = 'in-the-media';
    }

    // Post types and the menu items they relate to.
    switch ($post_type) {
        case 'page':
            $root_parent_title = get_post_field('post_name', get_top_parent_id());

            $page_url = '/' . $root_parent_title;
            break;

        // phpcs:ignore
        /*
        // phpcs:ignore
        case 'conference':
        case 'event':
            $page_url = '/events';
            break;

        case 'project':
            $page_url = '/research-and-ideas';
            break;

        case 'publication':
            $page_url = '/research-and-ideas/publications';
            break;

        case 'news':
            $page_url = '/news';
            break;

        case 'person':
            $page_url = '/about/staff';
            break;

        case 'vacancy':
            $page_url = '/about/jobs';
            break;

        case 'post':
            $page_url = '/blog';
            break;
        */

        default:
    }

    $url_in_item = preg_match('/^(' . preg_quote($item_title, '/') . ')(\/?$|\/.*$)/', $page_url);

    // If the menu item title is somewhere within the url
    // And the "current_page_parent" hasn't already been applied.
    if (!empty($url_in_item) && !in_array('current-page-ancestor', $classes, true) && !filter_input(INPUT_GET, 's', FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_AMP)) {
        // Add the "current_page_parent" class to the $classes array.
        array_push($classes, 'current-page-ancestor');
    } elseif ((empty($url_in_item) || filter_input(INPUT_GET, 's', FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_AMP)) && array_search('current-page-ancestor', $classes, true)) {
        unset($classes[array_search('current-page-ancestor', $classes, true)]);
    }

    return $classes;
}

add_filter('nav_menu_css_class', 'apply_parent_nav_class', 10, 2);

/**
 * Allow things like forms and iframes within wp_kses_post().
 *
 * @param array $allowed_html Array of allowed HTML tags.
 *
 * @return array
 */
function sbx_wp_kses_allowed_html(array $allowed_html): array
{

    $allowed_atts = array(
        'align' => array(),
        'class' => array(),
        'type' => array(),
        'id' => array(),
        'dir' => array(),
        'lang' => array(),
        'style' => array(),
        'xml:lang' => array(),
        'src' => array(),
        'srcset' => array(),
        'alt' => array(),
        'href' => array(),
        'rel' => array(),
        'rev' => array(),
        'target' => array(),
        'novalidate' => array(),
        'value' => array(),
        'name' => array(),
        'tabindex' => array(),
        'action' => array(),
        'method' => array(),
        'for' => array(),
        'width' => array(),
        'height' => array(),
        'data' => array(),
        'title' => array(),
    );

    $allowed_html['form'] = $allowed_atts;
    $allowed_html['label'] = $allowed_atts;
    $allowed_html['input'] = $allowed_atts;
    $allowed_html['textarea'] = $allowed_atts;
    $allowed_html['iframe'] = $allowed_atts;
    $allowed_html['script'] = $allowed_atts;
    $allowed_html['style'] = $allowed_atts;
    $allowed_html['strong'] = $allowed_atts;
    $allowed_html['small'] = $allowed_atts;
    $allowed_html['table'] = $allowed_atts;
    $allowed_html['span'] = $allowed_atts;
    $allowed_html['abbr'] = $allowed_atts;
    $allowed_html['code'] = $allowed_atts;
    $allowed_html['pre'] = $allowed_atts;
    $allowed_html['div'] = $allowed_atts;
    $allowed_html['img'] = $allowed_atts;
    $allowed_html['h1'] = $allowed_atts;
    $allowed_html['h2'] = $allowed_atts;
    $allowed_html['h3'] = $allowed_atts;
    $allowed_html['h4'] = $allowed_atts;
    $allowed_html['h5'] = $allowed_atts;
    $allowed_html['h6'] = $allowed_atts;
    $allowed_html['ol'] = $allowed_atts;
    $allowed_html['ul'] = $allowed_atts;
    $allowed_html['li'] = $allowed_atts;
    $allowed_html['em'] = $allowed_atts;
    $allowed_html['hr'] = $allowed_atts;
    $allowed_html['br'] = $allowed_atts;
    $allowed_html['tr'] = $allowed_atts;
    $allowed_html['td'] = $allowed_atts;
    $allowed_html['p'] = $allowed_atts;
    $allowed_html['a'] = $allowed_atts;
    $allowed_html['b'] = $allowed_atts;
    $allowed_html['i'] = $allowed_atts;
    $allowedtags['picture'] = $allowed_atts;
    $allowedtags['video'] = $allowed_atts;
    $allowedtags['source'] = $allowed_atts;

    return $allowed_html;
}

add_filter('wp_kses_allowed_html', 'sbx_wp_kses_allowed_html');

// Disable Emoji Scripts
// - https://crunchify.com/not-using-emoji-on-your-wordpress-blog-stop-loading-wp-emoji-release-min-js-and-css-file/ .
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * Disable oEmbed of YOUR site on OTHER sites
 * - https://crunchify.com/how-to-disable-auto-embed-script-for-wordpress-4-4-wp-embed-min-js/
 * -----------------------------------------------
 * Remove jQuery Migrate Script from header and Load jQuery from Google API.
 */
function sbx_stop_loading_wp_embed_and_jquery()
{
    if (!is_admin()) {
        wp_deregister_script('wp-embed');
    }
}

add_action('init', 'sbx_stop_loading_wp_embed_and_jquery');

// Remove the REST API endpoint.
//remove_action( 'rest_api_init', 'wp_oembed_register_route' );
add_filter('json_enabled', '__return_false');
add_filter('json_jsonp_enabled', '__return_false');
// Turn off oEmbed auto discovery.
add_filter('embed_oembed_discover', '__return_false');

// Don't filter oEmbed results.
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

// Remove oEmbed discovery links.
remove_action('wp_head', 'wp_oembed_add_discovery_links');

// Remove oEmbed-specific JavaScript from the front-end and back-end.
remove_action('wp_head', 'wp_oembed_add_host_js');

// Remove all embeds rewrite rules.
if (function_exists('disable_embeds_rewrites')) {
    add_filter('rewrite_rules_array', 'disable_embeds_rewrites');
}

/**
 * Disable Gutenbergs block-library CSS
 * - https://wpassist.me/2019/01/10/how-to-remove-block-library-css-from-wordpress/ .
 */
function sbx_remove_block_library_css()
{
    wp_dequeue_style('wp-block-library');
}

add_action('wp_enqueue_scripts', 'sbx_remove_block_library_css');
