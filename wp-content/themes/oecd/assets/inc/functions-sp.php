<?php
/**
 * File of useful functions SP like to use.
 */

/**
 * Template buffering wrapper for get_template_part() to save the output to a variable.
 *
 * @param string $template_name The template file to load.
 * @param null $part_name We don't use this but I've kept it to match the syntax from WP core.
 * @param array $args An array of variables to pass to the template.
 *
 * @return false|string
 */
function load_template_part(string $template_name, $part_name = null, array $args = array())
{

    // Start output buffering.
    ob_start();

    get_template_part($template_name, $part_name, $args);

    // Get the output from the buffer.
    $output = ob_get_contents();

    // End and clean the buffer.
    ob_end_clean();

    return $output;
}

/**
 * Join a list if items with, comma, comma, and put and before the last one.
 * - e.g. John Smith, John Smith and John Smith
 *
 * @param array $list
 * @param string $conjunction
 *
 * @return mixed|string|null
 */
function natural_language_join(array $list, $conjunction = 'and')
{

    $last = array_pop($list);

    if ($list) {
        return implode(', ', $list) . ' ' . $conjunction . ' ' . $last;
    }

    return $last;
}

/**
 * @param      $url
 * @param bool $internal
 *
 * @return false|string|null
 */
function get_file_size($url, $internal = true)
{

    if ($internal) {
        $path = wp_parse_url($url, PHP_URL_PATH);
        $server_path = $_SERVER['DOCUMENT_ROOT'] . $path;

        if (file_exists($server_path)) {
            return size_format(filesize($server_path));
        }

        return null;
    }

    // phpcs:disable
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);

    $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
    // phpcs:enable

    if ($size > 0) {
        return size_format($size);
    }

    return null;
}

/**
 * @param $pid
 *
 * @return mixed|string
 */
function get_event_date($pid)
{
    // Date Formats.
    $date_format = get_option('date_format');
    $time_format = 'g:i a';

    // Values.
    $start_datetime = get_field('start_date', $pid, false);
    $end_datetime = get_field('end_date', $pid, false);

    // Placeholders.
    $event_start_date = '';
    $event_start_time = '';
    $event_end_date = '';
    $event_end_time = '';

    // Event Start Date.
    $event_start = \DateTime::createFromFormat('Y-m-d H:i:s', $start_datetime);

    if ($event_start instanceof DateTime) {
        $event_start_date = $event_start->format($date_format);
        $event_start_time = $event_start->format($time_format);
    }

    // Event End Date.
    $event_end = \DateTime::createFromFormat('Y-m-d H:i:s', $end_datetime);

    if ($event_end instanceof DateTime) {
        $event_end_date = $event_end->format($date_format);
        $event_end_time = $event_end->format($time_format);
    }

    if (!get_field('show_times', $pid)) {
        $event_start_time = '';
        $event_end_time = '';
    }

    return prepare_event_date_string($event_start_date, $event_start_time, $event_end_date, $event_end_time);
}

/**
 * Format the event date without duplicating parts of the date.
 * - e.g.: 21 Oct 2021 3:00 pm - 5:00 pm
 *
 * @param        $start_date
 * @param        $start_time
 * @param        $end_date
 * @param        $end_time
 * @param string $join
 *
 * @return mixed|string
 */
function prepare_event_date_string($start_date, $start_time, $end_date, $end_time, $join = ' - ')
{
    $output = '';

    if (!empty($start_date) && !empty($end_date)) {
        if ($start_date === $end_date) {
            if ($start_time !== '12:00 am' && $end_time !== '12:00 am') {
                $output .= $start_date . ' ';

                if (!empty($start_time)) {
                    $output .= $start_time;
                }

                if (!empty($end_time)) {
                    $output .= $join . $end_time;
                }
            } else {
                $output .= $start_date . ' ' . $start_time;
            }
        } else {
            $output .= $start_date . $join . $end_date;
        }
    } elseif (!empty($start_date)) {
        $output = $start_date;

        if (!empty($start_time)) {
            $output .= ' - ' . $start_time;
        }
    }

    return $output;
}


/**
 * Get a human readable date time string.
 *
 * Examples:
 * - 9am, 24 September 2020 - 5pm, 3 January 2021
 * - 9am, 24 September - 5pm, 28 September 2020
 * - 24 - 28 September, 2020
 * - 24 September - 3 October, 2020
 * - 9am - 5pm, 24 September 2020
 * - 9am, 24 September 2020
 *
 * @param string $start_date Date in the desired format, must be readable by strtotime().
 * @param string $start_time Time in the desired format.
 * @param string $end_date Date in the desired format, must be readable by strtotime().
 * @param string $end_time Time in the desired format.
 * @param string $time_zone Time zone string (optional).
 *
 * @return string
 */
function get_human_readable_date_time_range_string($start_date, $start_time, $end_date, $end_time, $time_zone = ''): string
{
    // Config for date formats. Change these as desired.
    $day_format = 'j';
    $month_format = 'F';
    $year_format = 'Y';

    // Get individual date parts.
    $start_timestamp = strtotime($start_date);
    $start_day = gmdate($day_format, $start_timestamp);
    $start_month = gmdate($month_format, $start_timestamp);
    $start_year = gmdate($year_format, $start_timestamp);
    $end_timestamp = strtotime($end_date);
    $end_day = gmdate($day_format, $end_timestamp);
    $end_month = gmdate($month_format, $end_timestamp);
    $end_year = gmdate($year_format, $end_timestamp);

    if ($start_date && $end_date && $start_date !== $end_date) {
        if ($start_year !== $end_year) {
            // Different year, keep every bit of the date separate.
            // 9am, 24 September 2020 - 5pm, 3 January 2021.
            return trim(
                $start_time . ($start_time ? ', ' : '')
                . $start_day . ' ' . $start_month
                . ($start_year ? ' ' . $start_year : '')
                . ' - '
                . $end_time . ($end_time ? ', ' : '')
                . $end_day . ' ' . $end_month
                . ($end_year ? ' ' . $end_year : '')
                . '(' . $time_zone . ')'
            );
        } else {

            if ($start_time || $end_time) {
                // Shared year, but has times so keep months separate.
                // 9am, 24 September - 5pm, 28 September 2020.
                return trim(
                    $start_time . ($start_time ? ', ' : '')
                    . $start_day . ' ' . $start_month
                    . ' - '
                    . $end_time . ($end_time ? ', ' : '')
                    . $end_day . ' ' . $end_month
                    . ($end_year ? ' ' . $end_year : '')
                    . ($time_zone ? ' (' . $time_zone . ')' : '')
                );
            } elseif ($start_month === $end_month) {

                // Shared year and no times, combine months as they match.
                // 24 - 28 September, 2020.
                return trim(
                    $start_day . ' - ' . $end_day
                    . ' ' . $start_month
                    . ($start_year ? ' ' . $start_year : '')
                    . ($time_zone ? ' (' . $time_zone . ')' : '')
                );
            } else {

                // Shared year and no times, differing months months as they match.
                // 24 September - 3 October, 2020.
                return trim(
                    $start_day . ' ' . $start_month
                    . ' - '
                    . $end_day . ' ' . $end_month
                    . ($end_year ? ' ' . $end_year : '')
                    . ($time_zone ? ' (' . $time_zone . ')' : '')
                );
            }
        }
    } else {
        // Group start and end time together if we have both.
        if ($start_time && $end_time) {
            // 9am - 5pm, 24 September 2020.
            return trim(
                $start_time . ' - ' . $end_time . ', ' . $start_date
                . ($time_zone ? ' (' . $time_zone . ')' : '')
            );
        } else {
            // 9am, 24 September 2020.
            return trim(
                $start_time . $end_time . ' ' . $start_date
                . ($time_zone ? ' (' . $time_zone . ')' : '')
            );
        }
    }
}

/**
 * Function to generate args to get all content authored / linked to a person.
 *
 * @param $post_id
 *
 * @return array
 */
function get_person_content_args($post_id)
{
    return array(
        'fields' => 'ids',
        'post_type' => 'any',
        'post_status' => 'publish',
        // phpcs:ignore
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'compare' => 'LIKE',
                'key' => 'authors',
                'value' => '"' . $post_id . '"',
            ),
            // phpcs:disable
            // [
            // 	'compare' => 'LIKE',
            // 	'key'     => 'components_%_people',
            // 	'value'   => '"' . $post_id . '"',
            // ],
            // [
            //     'compare' => 'LIKE',
            //     'key'     => 'header_sidebar_contacts',
            //     'value'   => '"' . $post_id . '"',
            // ],
            // [
            //     'compare' => 'LIKE',
            //     'key'     => 'sidebar_content',
            //     'value'   => '"' . $post_id . '"',
            // ],
            // phpcs:enable
        ),
        'posts_per_page' => 8,
        // phpcs:ignore
        'meta_key' => get_option('custom_date_field', 'post_date'),
        'meta_type' => 'DATE',
        'orderby' => 'meta_value',
    );
}

/**
 * Modify the SQL before it is executed to convert related content meta_queries into wheres.
 *
 * @param $where
 *
 * @return mixed
 */
function person_where($where)
{

    global $wpdb;

    $where = str_replace("meta_key = 'components_%", "meta_key LIKE 'components_%", $wpdb->remove_placeholder_escape($where));

    return $where;
}

//add_filter( 'posts_where', 'person_where' );

/**
 * Get an array of slugs from an ACF taxonomy field.
 *
 * @param string $field_name
 * @param int $post_id
 *
 * @return array
 */
function get_slugs_from_taxonomy_field(string $field_name, int $post_id): array
{

    $tax_terms_to_show = get_field($field_name, $post_id);
    $tax_slugs = array();

    if (!empty($tax_terms_to_show)) {
        $tax_slugs = array_map(
            function ($object) {
                return $object->slug;
            },
            $tax_terms_to_show
        );
    }

    return $tax_slugs;
}

/**
 * Get the sb_post_date for a post.
 * - sb_post_date is the normalised date value flattened to improve cross content type sorting.
 *
 * @param int $post_id
 *
 * @return string
 */
function get_post_date(int $post_id): string
{
    // Get the sb_post_date field from the custom date settings.
    $post_date = get_field('sb_post_date', $post_id, false);

    // Create a DateTime instance.
    $date = DateTime::createFromFormat('Y-m-d H:i:s', $post_date);

    if (empty($date)) {
        $date = DateTime::createFromFormat('Y-m-d', $post_date);
    }

    if (empty($date)) {
        return '';
    }

    // Get the output.
    return $date->format(get_option('date_format'));
}

/**
 * Function to get array of All Authors OR Featured Authors
 *
 * @param      $post_id
 * @param bool $all
 *
 * @return array
 */
function get_authors($post_id)
{

    $authors = get_field('authors', $post_id);
    $authors_to_show = array();

    if (!empty($authors)) {
        foreach ($authors as $author_id) {
            $first_name = get_field('first_name', $author_id);
            $last_name = get_field('last_name', $author_id);

            if (empty($first_name) && empty($last_name)) {
                $name = get_the_title($author_id);
            } else {
                $name = $first_name . ' ' . $last_name;
            }

            $authors_to_show[] = array(
                'id' => $author_id,
                'name' => $name,
            );
        }
    }

    return $authors_to_show;
}

function get_authors_with_links($post_id)
{

    $authors = get_field('authors', $post_id);
    $authors_links = array();

    if (!empty($authors)) {
        foreach ($authors as $author_id) {
            $first_name = get_field('first_name', $author_id);
            $last_name = get_field('last_name', $author_id);
            $link = get_permalink($author_id);

            if (empty($first_name) && empty($last_name)) {
                $name = get_the_title($author_id);
            } else {
                $name = $first_name . ' ' . $last_name;
            }

            $author_links[] = '<a href="' . $link . '" title="View ' . $name . '\'s profile">' . $name . '</a>';
        }
    }

    return natural_language_join($author_links);
}

/**
 * Function to return the post type taking into account each of the subtype taxonomies.
 *
 * @param $post_id
 *
 * @return mixed
 */
function get_post_type_label($post_id = null)
{

    if (empty($post_id)) {
        $post_id = get_the_ID();
    }

    $post_type = get_post_type($post_id);
    $post_type_obj = get_post_type_object($post_type);

    switch ($post_type) {
        // phpcs:disable

        case 'resource':
            $post_type_label = get_first_taxonomy_label($post_id, 'resource-subtype');
            break;

        case 'event':
            $post_type_label = get_first_taxonomy_label($post_id, 'event-subtype');
            break;

        case 'organisation':
            $post_type_label = get_first_taxonomy_label($post_id, 'organisation-subtype');
            break;
        // phpcs:enable

        default:
            if (!empty($post_type_obj)) {
                $post_type_label = $post_type_obj->labels->singular_name;
            }
            break;
    }

    if (empty($post_type_label)) {
        $post_type_label = '';
    }

    return $post_type_label;
}

/**
 * Get a post's taxonomy type - return the first one if more than one
 *
 * @param $post_id
 * @param $taxonomy
 *
 * @return mixed
 */
function get_first_taxonomy_label($post_id, $taxonomy)
{

    $post_type = get_post_type($post_id);
    $post_type_obj = get_post_type_object($post_type);

    $tax_terms = wp_get_post_terms($post_id, $taxonomy);

    if (!empty($tax_terms[0])) {
        $post_type_label = $tax_terms[0]->name;
    } else {
        $post_type_label = $post_type_obj->labels->singular_name;
    }

    return $post_type_label;
}

/**
 * Return an array of terms related to a post
 *
 * @param        $post_id
 * @param        $tax
 * @param string $value
 *
 * @return array
 */
function get_post_tax_terms($post_id, $tax, string $value = 'name')
{

    $term_objects = wp_get_post_terms($post_id, $tax);
    $terms = array();

    if (!empty($term_objects)) {
        foreach ($term_objects as $term) {
            $terms[] = $term->$value;
        }
    }

    return $terms;
}

/**
 * Array of objects to array of Object-Property strings
 *
 * @param $array
 * @param $property
 *
 * @return array
 */
function object_strings($array, $property)
{

    $results = [];

    foreach ($array as $object) {
        $results[] = $object->$property;
    }

    return $results;
}

/**
 * Function to Obfuscate emails against spam.
 *
 * @param mixed $email The email address you want to obfuscate.
 *
 * @return string the obfuscated email string
 */
function protect_email($email): string
{

    $new_mail = '';

    if (!empty($email)) {
        $p = str_split(trim($email));

        foreach ($p as $val) {
            $new_mail .= '&#' . ord($val) . ';';
        }
    }

    return $new_mail;
}

/**
 * Get Top Parent Post Id
 */
function get_top_parent_id($post_id = null)
{
    global $post;

    if (!empty($post_id)) {
        $post = get_post($post_id);
    }

    $parent = $post->ID;

    if ($post->post_parent) {
        $ancestors = get_post_ancestors($post->ID);
        $root = count($ancestors) - 1;
        $parent = $ancestors[$root];
    }

    return $parent;
}

/**
 * @param string $text Which is to be shortened.
 * @param int $length Length by which to shorten the string.
 * @param string $ending String to show at the end.
 * @param bool $exact Whether to cut words in the middle or not.
 * @param bool $consider_html Whether to close all open HTML tags.
 *
 * @return string the substring
 */
function _html_substr($text, $length = 100, $ending = '...', $exact = false, $consider_html = true)
{

    if ($consider_html) {
        // If the plain text is shorter than the maximum length, return the whole text.
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }

        // splits all html-tags to scanable lines.
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';

        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output.
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash (f.e. <br/>).
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag (f.e. </b>).
                } elseif (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list.
                    $pos = array_search($tag_matchings[1], $open_tags, true);
                    if (false !== $pos) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag (f.e. <b>).
                } elseif (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list.
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }

                // add html-tag to $truncate'd text.
                $truncate .= $line_matchings[1];
            }

            // calculate the length of the plain text part of the line; handle entities as one character.
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));

            if ($total_length + $content_length > $length) {
                // the number of characters which are left.
                $left = $length - $total_length;
                $entities_length = 0;

                // search for html entities.
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range.
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left --;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left.
                            break;
                        }
                    }
                }

                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum length is reached, so get off the loop.
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }

            // if the maximum length is reached, get off the loop.
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }

    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space..
        $spacepos = strrpos($truncate, ' ');

        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }

    // add the defined ending to the text.
    $truncate .= $ending;

    if ($consider_html) {
        // close all unclosed html-tags.
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }

    return $truncate;
}

/**
 * Generate a custom excerpt at the specified length and append with ellipses if necessary.
 *
 * @param mixed $excerpt The excerpt string.
 * @param mixed $content The body content string.
 * @param int $char_length Determines the length of the excerpt.
 * @param bool $trim_excerpt Determines whether to shorten the excerpt if it is more than $char_length.
 *
 * @return string The custom excerpt string.
 */
function custom_the_excerpt($excerpt = '', $content = '', int $char_length = 300, bool $trim_excerpt = false): string
{
    $excerpt = strip_tags($excerpt, '<br><br/><br />');
    $content = strip_tags($content, '<br><br/><br />');

    $excerpt = html_entity_decode($excerpt, ENT_QUOTES | ENT_HTML5, 'UTF-8');
    $content = html_entity_decode($content, ENT_QUOTES | ENT_HTML5, 'UTF-8');

    if (empty($excerpt) && !empty($content) && strlen($content) > 0) {
        if (strlen($content) > $char_length) {
            $html = _html_substr($content, $char_length, '...', false, true);
        } else {
            $html = _html_substr($content, strlen($content), '', false, true);
        }
    } elseif (!empty($excerpt) && $trim_excerpt) {
        if (strlen($excerpt) > $char_length) {
            $html = _html_substr($excerpt, $char_length, '...', false, true);
        } else {
            $html = _html_substr($excerpt, strlen($excerpt), '', false, true);
        }
    } else {
        $html = _html_substr($excerpt, strlen($excerpt), '', false, true);
    }

    return $html;
}

/**
 * Get the slug of the post.
 *
 * @return String the post's slug
 */
function get_slug()
{
    global $post;

    $post_data = get_post($post->ID, ARRAY_A);

    return $post_data['post_name'];
}

/**
 * Echo the post's slug
 */
function the_slug()
{

    global $post;
    $post_data = get_post($post->ID, ARRAY_A);
    $slug = $post_data['post_name'];

    echo esc_attr($slug);
}

/**
 * Generate a cropped excerpt from page builder components.
 *
 * @param int|null $post_id
 * @param int $length
 *
 * @return string
 */
function get_page_builder_excerpt($post_id = null, $length = 200): string
{

    if (empty($post_id)) {
        $post_id = get_the_ID();
    }

    $excerpt = '';
    $post_content = get_post_field('post_content', $post_id);
    $post_excerpt = get_field('description', $post_id);
    $content_blocks = get_field('components', $post_id);

    if (!empty($post_content)) {
        $excerpt = custom_the_excerpt($post_excerpt, $post_content, $length, true);
    } elseif (!empty($content_blocks) && is_array($content_blocks)) {
        foreach ($content_blocks as $block) {
            if ($block['acf_fc_layout'] === 'text_block') {
                $excerpt = custom_the_excerpt($post_excerpt, $block['wysiwyg_editor'], $length, true);

                break;
            }
        }
    }

    return $excerpt;
}

/**
 * Get a basic array of menu items.
 *
 * @param $menu_slug
 *
 * @return array|false
 */
function get_menu_items($menu_slug)
{

    $menu_locations = get_nav_menu_locations();

    if (!empty($menu_locations[$menu_slug])) {
        $menu_id = $menu_locations[$menu_slug];

        return wp_get_nav_menu_items($menu_id);
    }

    return array();
}

/**
 * This is pagination bar functionality.
 */
function pagination_bar($query)
{

    $total_pages = $query->max_num_pages;

    if ($total_pages > 1) {
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text' => '<span class="c-list__pagination-prev">Previous</span>',
            'next_text' => '<span class="c-list__pagination-next">Next</span>',
        ));
    }
}

function sortByName($a, $b)
{
    return $a > $b;
}

//Page Slug Body Class
function add_slug_body_class($classes)
{
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}

add_filter('body_class', 'add_slug_body_class');
