<?php
/**
 * Declare custom post types in this file!
 */

/**
 * Register the custom post types
 */
function sbx_register_cpts() {
	register_reports();
}

add_action( 'init', 'sbx_register_cpts', 0 );


/**
 * Individual post type functions
 */
function register_reports() {
	$singular = 'Report';
	$plural   = 'Reports';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Parent ' . $singular, 'text_domain' ),
		'all_items'          => __( 'All ' . $plural, 'text_domain' ),
		'view_item'          => __( 'View ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Add New ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Add New', 'text_domain' ),
		'edit_item'          => __( 'Edit ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Update ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Search ' . $singular, 'text_domain' ),
		'not_found'          => __( 'Not Found', 'text_domain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'My list of ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			'editor',
			'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			'thumbnail',
			// phpcs:enable
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 2,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-media-document',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}

/**
 * Individual post type functions
 */
function register_organisations() {
	$singular = 'Organisation';
	$plural   = 'Organisations';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Parent ' . $singular, 'text_domain' ),
		'all_items'          => __( 'All ' . $plural, 'text_domain' ),
		'view_item'          => __( 'View ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Add New ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Add New', 'text_domain' ),
		'edit_item'          => __( 'Edit ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Update ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Search ' . $singular, 'text_domain' ),
		'not_found'          => __( 'Not Found', 'text_domain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'My list of ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			'editor',
			'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			//'thumbnail',
			// phpcs:enable
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 2,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-building',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}

/**
 * Change query vars if the user is trying to access a subpage of a post type.
 * (Avoid this situation if possible, the WP routing isn't great...).
 */
/*
add_filter(
	'request',
	function ( $query_vars ) {
		if ( ! isset( $query_vars['post_type'] ) || ! isset( $query_vars['blog'] ) ) {
			return $query_vars;
		}

		// Find a blog post page exactly matching Blog.
		$page = get_page_by_title( 'Blog' );

		if ( 'blog' === $query_vars['post_type'] && ! empty( $page ) ) {
			$children = get_posts(
				array(
					'post_type'   => 'page',
					'post_parent' => $page->ID,
				)
			);

			if ( ! empty( $children ) ) {
				$sub_page_slugs = wp_list_pluck( $children, 'post_name' );

				if ( in_array( $query_vars['blog'], $sub_page_slugs, true ) ) {
					$query_vars = array(
						'page'      => '',
						'name'      => $query_vars['blog'],
						'post_type' => 'page',
						'pagename'  => $query_vars['blog'],
					);

					add_action( 'pre_get_posts', 'sub_page_query_alterations' );
				}
			}
		}

		return $query_vars;
	}
);
*/

/**
 * Alter the wp query for sub pages.
 *
 * @param WP_Query $query
 */
/*
function sub_page_query_alterations( WP_Query $query ) {
	if ( $query->is_main_query() ) {
		$query->is_single = false;
		$query->is_page   = true;
	}
}
*/

/**
 * Add custom post type counts to the at a glance box
 */
function cpt_at_glance() {
	$args       = array(
		'public'   => true,
		'_builtin' => false,
	);
	$output     = 'object';
	$operator   = 'and';
	$post_types = get_post_types( $args, $output, $operator );

	foreach ( $post_types as $post_type ) {
		$num_posts = wp_count_posts( $post_type->name );
		$num       = number_format_i18n( $num_posts->publish );
		$text      = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );

		if ( current_user_can( 'edit_posts' ) ) {
			echo '<li class="custom-type-count ' . esc_attr( $post_type->name ) . '-count">';
			echo '<a href="edit.php?post_type=' . esc_attr( $post_type->name ) . '">' . esc_textarea( $num . ' ' . $text ) . '</a>';
			echo '</li>';
		}
	}
}

add_action( 'dashboard_glance_items', 'cpt_at_glance' );
