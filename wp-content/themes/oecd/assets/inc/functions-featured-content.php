<?php
/**
 * Functions to manage featured content selection.
 */

/**
 * Overarching function to retrieve manual and auto featured content
 *
 * @param array $manual_featured_content
 * @param boolean $auto_enabled
 * @param array $contenttypes_to_feature
 * @param array $extra_args
 * @param int $number_of_items
 * @param array $excluded_posts
 *
 * @return array
 */
function get_featured_content(
    array $manual_featured_content = array(),
    bool  $auto_enabled = true,
    array $contenttypes_to_feature = array(),
    int   $number_of_items = 3,
    array $extra_args = array(),
    array $excluded_posts = array()
): array
{

    // The current post.
    $post_id = get_the_ID();

    // Check how many manual items are shown.
    $number_of_manual_items = count($manual_featured_content);

    if ($number_of_manual_items > $number_of_items) {
        $number_of_items = $number_of_manual_items;
    }

    foreach ($manual_featured_content as $k => $post_id) {
        if (get_post_status($post_id) !== 'publish') {
            unset($manual_featured_content[$k]);
        }
    }

    // Merge the excluded posts array with the array of manual content to avoid duplicates.
    $excluded_posts = array_merge($excluded_posts, $manual_featured_content);

    // Exclude the current item from appearing.
    $excluded_posts[] = $post_id;

    // If not enough manual items are set, get some auto featured content.
    if (($number_of_manual_items < $number_of_items) && false !== $auto_enabled) {
        $auto_featured_content = get_auto_featured_content($contenttypes_to_feature, $number_of_items, $excluded_posts, $extra_args);
    } else {
        $auto_featured_content = array();
    }

    $featured_content = array_merge($manual_featured_content, $auto_featured_content);

    // Limit the array to the number requested.
    $featured_content = array_slice($featured_content, 0, $number_of_items, true);
    return $featured_content;
}

/**
 * Function to retrieve and manage the automatically generated content
 *
 * @param int $limit
 * @param array $excluded_posts
 *
 * @return array
 */
function get_auto_featured_content(
    $contenttypes_to_feature,
    $limit = 6,
    $excluded_posts = [],
    $extra_args = []
)
{
    if (empty($contenttypes_to_feature)) {
        $contenttypes_to_feature = [
            'post',
            'report',
            'page'
        ];
    }

    // Base related args
    $auto_featured_args = [
        'fields' => 'ids',
        'post_type' => $contenttypes_to_feature,
        'posts_per_page' => $limit,
        'post_status' => 'publish',
        'post__not_in' => $excluded_posts,
        'order' => 'DESC',
    ];

    $auto_featured_args = array_merge($auto_featured_args, $extra_args);
    $auto_featured_content = new WP_Query($auto_featured_args);

    return $auto_featured_content->posts;
}
