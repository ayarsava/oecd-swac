<?php
/**
 * Advanced custom fields customisations, hooks and filters.
 */

/**
 * Make sure the ACF Pro plugin is active (the theme requires it).
 */
if ( function_exists( 'is_plugin_active' ) ) {
	$is_acf_active = is_plugin_active( 'advanced-custom-fields-pro/acf.php' );

	if ( ! $is_acf_active ) {
		activate_plugin( 'advanced-custom-fields-pro/acf.php' );
	}
}

/**
 * Enable the ACF options page
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( [
		'page_title' => 'Options',
		'slug'       => 'custom-options',
		'capability' => 'manage_options',
	] );

	acf_add_options_sub_page( [
		'title'      => 'Contact Settings',
		'slug'       => 'contact-settings',
		'parent'     => 'custom-options',
		'capability' => 'manage_options',
	] );

	acf_add_options_sub_page( [
		'title'      => 'Content Settings',
		'slug'       => 'content-settings',
		'parent'     => 'custom-options',
		'capability' => 'manage_options',
	] );
}

add_filter( 'acf/fields/wysiwyg/toolbars', 'my_toolbars' );
function my_toolbars( $toolbars ) {
	// Add a new toolbar called "Very Simple"
	// - this toolbar has only 1 row of buttons
	$toolbars['Only bold']    = array();
	$toolbars['Only bold'][1] = array( 'bold' );

	// Edit the "Full" toolbar and remove 'code'
	// - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
	if ( ( $key = array_search( 'code', $toolbars['Full'][2] ) ) !== false ) {
		unset( $toolbars['Full'][2][ $key ] );
	}

	// remove the 'Basic' toolbar completely
	//unset( $toolbars['Basic'] );

	// return $toolbars - IMPORTANT!
	return $toolbars;
}

