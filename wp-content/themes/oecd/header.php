<?php
/**
 * Header template
 */
?>
	<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta id="viewport" name="viewport" content="width=device-width">

		<?php
		// Sharing Meta
		if ( ! function_exists( 'wpseo_init' ) ) {
			// Get the username from the Twitter URL
			$twitter_username = get_field( 'twitter', 'options' );
			$favicon_image    = get_template_directory_uri() . '/assets/img/favicon.png';
			if ( ( is_single() || is_page() ) && ! is_search() && ! is_front_page() ) {
				$page_title  = get_the_title();
				$excerpt     = get_post_field( 'post_excerpt', get_the_ID() );
				$content     = get_post_field( 'post_content', get_the_ID() );
				$description = get_page_builder_excerpt( get_the_ID() );

				$thumb_id        = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'medium', true );
				$featured_image  = $thumb_url_array[0];

				if ( strpos( $featured_image, 'default.png' ) ) {
					$featured_image = get_template_directory_uri() . '/assets/img/mcfi-share-image.png';
				}
			} else {
				$page_title     = get_bloginfo( 'name' );
				$description    = get_bloginfo( 'description', 'raw' );
				$featured_image = get_template_directory_uri() . '/assets/img/mcfi-share-image.png';
			}
			?>

			<link rel="sitemap" type="application/xml" title="Sitemap" href="/sitemap.xml">

			<?php if ( is_search() ) { // Canonical for search pages should be without GET params. ?>
				<link rel="canonical" href="<?php echo esc_url( site_url() ); ?>search/"/>
			<?php } ?>
			<link rel="icon" type="image/png" href="<?php echo esc_attr( $favicon_image ); ?>">
			<meta name="description" content="<?php echo esc_attr( $description ); ?>">

			<!-- Facebook / OpenGraph -->
			<meta property="og:type" content="website"/>
			<meta property="og:url" content="<?php echo esc_url( get_permalink() ); ?>"/>
			<meta property="og:title" content="<?php echo esc_attr( $page_title ); ?>"/>
			<meta property="og:description" content="<?php echo esc_attr( $description ); ?>"/>
			<meta property="og:image" content="<?php echo esc_attr( $featured_image ); ?>"/>

			<!-- Twitter -->
			<meta name="twitter:card" content="summary_large_image">

			<?php
			if ( ! empty( $twitter_username ) ) {
				?>
				<meta name="twitter:site" content="@<?php echo esc_attr( $twitter_username ); ?>">
				<?php
			}
			?>

			<meta name="twitter:title" content="<?php echo esc_attr( $page_title ); ?>"/>
			<meta name="twitter:description" content="<?php echo esc_attr( $description ); ?>"/>
			<meta name="twitter:image" content="<?php echo esc_attr( $featured_image ); ?>">

			<title><?php wp_title( '|', true, 'right' ); ?></title>
			<?php
		}
		?>

		<!-- WordPress head functions -->
		<?php wp_head(); ?>
		<!--[if IE]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->

	</head>

<body <?php body_class(); ?>>
<?php
get_template_part( 'assets/views/header' );
