<?php
/**
 * OECD functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package OECD
 */

// CMS configs.
require_once get_template_directory() . '/assets/inc/functions-config.php';


// Backend / admin area configuration.
require_once get_template_directory() . '/assets/inc/functions-config-admin.php';


// Custom post types.
require_once get_template_directory() . '/assets/inc/functions-post-types.php';


// Custom taxonomies.
require_once get_template_directory() . '/assets/inc/functions-taxonomies.php';


// Customise the login form.
require_once get_template_directory() . '/assets/inc/functions-login.php';


// SP common functions.
require_once get_template_directory() . '/assets/inc/functions-sp.php';


/**
 * Custom frontend functionality
 */

// Featured content selection logic.
require_once get_template_directory() . '/assets/inc/functions-featured-content.php';

// Related content selection logic.
require_once get_template_directory() . '/assets/inc/functions-related-content.php';


/**
 * Plugin customisations and extending plugin functionality
 */
// Advanced Custom Fields / ACF.
require_once get_template_directory() . '/assets/inc/functions-acf.php';
