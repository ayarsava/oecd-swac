<?php
/**
 * Template Name: Listing Page
 */

get_header();

while ( have_posts() ) {
	the_post();
	get_template_part( 'assets/views/page-header' );
	get_template_part( 'assets/views/content-blocks' );
}

get_footer();
