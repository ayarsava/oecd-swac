<?php
/**
 * Footer template
 */

?>
</main>

<footer class="c-footer">

    <div class="o-container c-footer__inner">

        <div class="c-footer__menu">
            <div class="c-footer__menu-wrapper">
                <div class="c-footer__social">
                    <?php get_template_part('assets/views/partials/social-links'); ?>
                </div>
                <?php
                $footer_legal_text = get_field('footer_legal_text', 'options');
                $footer_legal_text_french = get_field('footer_legal_text_french', 'options');
                if (!empty($footer_legal_text) || !empty($footer_legal_text_french)) {
                    if ('en' != pll_current_language() && !empty($footer_legal_text_french)) {
                        ?>
                        <p class="c-footer__legal-text"><?php echo esc_html($footer_legal_text_french); ?></p>
                        <?php
                    } else {
                        ?>
                        <p class="c-footer__legal-text"><?php echo esc_html($footer_legal_text); ?></p>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

        <div class="c-footer__newsletter">
            <?php
            // Newsletter vars.
            $newsletter_link = get_field('newsletter_link', 'options');
            $newsletter_footer_title = get_field('newsletter_footer_title', 'options');
            $newsletter_footer_button_text = get_field('newsletter_footer_button_text', 'options');
            ?>
            <div class="c-footer__newsletter-inner">
                <?php
                if (!empty($newsletter_footer_title)) {
                    ?>
                    <h4 class="c-footer__newsletter-title"><?php echo esc_html($newsletter_footer_title); ?></h4>
                    <?php
                }
                ?>
                <?php
                if (!empty($newsletter_link)) {
                    ?>
                    <a href="<?php echo esc_url($newsletter_link); ?>"
                       class="c-footer__newsletter-btn" target="_blank">
                        <?php
                        if (!empty($newsletter_footer_button_text)) {
                            echo esc_html($newsletter_footer_button_text);
                        } else {
                            echo 'Subscribe';
                        }
                        ?>
                    </a>
                    <?php
                }
                ?>

                <p class="c-footer__copyright">
                    ©<?php echo date("Y"); ?>. SWAC/OECD
                </p>
            </div>
        </div>

        <img class="c-footer__inner-image"
             src="<?php echo get_template_directory_uri(); ?>/assets/img/footer-pattern.png">
    </div>

</footer>

<?php
wp_footer();
?>
</body>
</html>
