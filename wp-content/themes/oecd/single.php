<?php
/**
 * Single post template.
 */

$full_width = get_field( 'full_width_content_layout' );
get_header();

// Start the Loop.
while ( have_posts() ) {
	the_post();

	global $post_id;
	?>
	<!-- Main content -->
	<?php
	get_template_part( 'assets/views/single-header' );
	?>

	<article class="o-single">
		<div class="o-single__main<?php if ( $full_width ) {
			echo ' o-single__main-full-width';
		} ?>">
			<?php get_template_part( 'assets/views/content-blocks' ); ?>
		</div>
	</article>

	<?php get_template_part( 'assets/views/related-content' ); ?>

	<?php
}
get_footer();
