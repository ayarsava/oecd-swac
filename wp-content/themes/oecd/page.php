<?php

$full_width = get_field( 'full_width_content_layout' );
get_header();

while ( have_posts() ) {
	the_post();

	get_template_part( 'assets/views/page-header' );
	get_template_part( 'assets/views/sub-nav' );

	?>
	<div class="o-page<?php if ( $full_width ) {
		echo ' o-page__main-full-width';
	} ?>">
		<?php
		get_template_part( 'assets/views/content-blocks' );
		?>
	</div>
	<?php

}

get_footer();
